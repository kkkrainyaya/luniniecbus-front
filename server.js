const express = require('express');

const app = express();
const port = 3080;

app.use(express.static(process.cwd()));

app.get('/*', (req, res) =>
  res.sendFile(process.cwd() + '/index.html'),
);

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`)
  }
);
