import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AboutComponent} from './components/about/about.component';
import {BannerComponent} from './components/banner/banner.component';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {HomeComponent} from './components/home/home.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BookingFormComponent} from './components/booking-form/booking-form.component';
import {IConfig, NgxMaskModule} from 'ngx-mask';
import {LoginComponent} from './components/login/login.component';
import {OrdersComponent} from './components/profile/orders/orders.component';
import {InfoComponent} from './components/profile/info/info.component';
import {RoutesComponent} from './components/admin/routes/routes.component';
import {AuthInterceptor} from './helpers/auth.interceptor';
import {NgxSimpleCountdownModule} from 'ngx-simple-countdown';
import {RouteConstants} from './constants/route-constants';
import {UnauthorizedGuard} from './guards/unauthorized.guard';
import {AuthorizedGuard} from './guards/authorized.guard';
import {UserIdleModule} from 'angular-user-idle';
import {TokenStorageService} from './services/token-storage.service';
import {AdminComponent} from './components/admin/admin.component';
import {PassengersComponent} from './components/dispatcher/passengers/passengers.component';
import {TableModule} from 'primeng/table';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {EditPassengerFormComponent} from './components/edit-passenger-form/edit-passenger-form.component';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {InputSwitchModule} from 'primeng/inputswitch';
import {ToolbarModule} from 'primeng/toolbar';
import {CalendarModule} from 'primeng/calendar';
import {AdminScheduleComponent} from './components/admin/schedule/admin-schedule.component';
import {FullCalendarModule} from '@fullcalendar/angular';
import {GeneralScheduleComponent} from './components/schedule/general-schedule.component';
import {StaffComponent} from './components/admin/staff/staff.component';
import {TooltipModule} from 'primeng/tooltip';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {BusesComponent} from './components/admin/buses/buses.component';
import {DispatcherComponent} from './components/dispatcher/dispatcher.component';
import {AdminGuard} from './guards/admin.guard';
import {StaffScheduleComponent} from './components/admin/staff-schedule/staff-schedule.component';
import {DatePipe} from '@angular/common';
import {PassengersAverageComponent} from './components/admin/statistics/passengers-average/passengers-average.component';
import {ChartModule} from 'primeng/chart';
import {DateWithDayPipe} from './helpers/date-with-day.pipe';
import {GoogleAnalyticsService} from './services/google-analytics.service';
import {HelpComponent} from './components/help/help.component';
import {BookingComponent} from './components/help/booking/booking.component';
import {BookingInfoComponent} from './components/help/booking-info/booking-info.component';
import {BookingApprovalComponent} from './components/help/booking-approval/booking-approval.component';
import {BookingUpdateComponent} from './components/help/booking-update/booking-update.component';
import {BookingAnotherPointComponent} from './components/help/booking-another-point/booking-another-point.component';
import {BookingCancelComponent} from './components/help/booking-cancel/booking-cancel.component';
import {InfoUpdateComponent} from './components/help/info-update/info-update.component';
import {PasswordComponent} from './components/help/password/password.component';
import {PhoneComponent} from './components/help/phone/phone.component';
import {PrivacyComponent} from './components/privacy/privacy.component';
import {BookingApprovalHowComponent} from './components/help/booking-approval-how/booking-approval-how.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

const routes: Routes = [
    {
        path: RouteConstants.ROOT.ABOUT,
        component: AboutComponent
    },
    {
        path: RouteConstants.ROOT.PRIVACY,
        component: PrivacyComponent
    },
    {
        path: RouteConstants.ROOT.SCHEDULE,
        component: GeneralScheduleComponent
    },
    {
        path: RouteConstants.ROOT.HELP,
        component: HelpComponent
    },
    {
        path: 'help/booking',
        component: BookingComponent,
    },
    {
        path: 'help/booking-info',
        component: BookingInfoComponent,
    },
    {
        path: 'help/booking-approval',
        component: BookingApprovalComponent,
    },
    {
        path: 'help/booking-approval-how',
        component: BookingApprovalHowComponent,
    },
    {
        path: 'help/booking-update',
        component: BookingUpdateComponent,
    },
    {
        path: 'help/booking-another-point',
        component: BookingAnotherPointComponent,
    },
    {
        path: 'help/booking-cancel',
        component: BookingCancelComponent,
    },
    {
        path: 'help/info-update',
        component: InfoUpdateComponent,
    },
    {
        path: 'help/password',
        component: PasswordComponent,
    },
    {
        path: 'help/phone',
        component: PhoneComponent,
    },
    {
        path: RouteConstants.ROOT.LOGIN,
        component: LoginComponent,
        canLoad: [UnauthorizedGuard]
    },
    {
        path: RouteConstants.PERSONAL.ORDERS,
        component: OrdersComponent,
        canActivate: [AuthorizedGuard]
    },
    {
        path: RouteConstants.PERSONAL.PROFILE,
        component: InfoComponent,
        canActivate: [AuthorizedGuard]
    },
    {
        path: RouteConstants.ROOT.ADMIN,
        component: AdminComponent,
        canActivate: [AdminGuard]
    },
    {
        path: RouteConstants.ROOT.DISPATCHER,
        component: DispatcherComponent,
        canActivate: [AdminGuard]
    },
    {
        path: RouteConstants.ROOT.STATISTICS,
        component: PassengersAverageComponent,
        canActivate: [AdminGuard]
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        AboutComponent,
        BannerComponent,
        FooterComponent,
        HeaderComponent,
        HomeComponent,
        AdminScheduleComponent,
        SearchFormComponent,
        BookingFormComponent,
        LoginComponent,
        OrdersComponent,
        InfoComponent,
        RoutesComponent,
        AdminComponent,
        PassengersComponent,
        EditPassengerFormComponent,
        AdminScheduleComponent,
        StaffComponent,
        BusesComponent,
        DispatcherComponent,
        StaffScheduleComponent,
        PassengersAverageComponent,
        DateWithDayPipe,
        GeneralScheduleComponent,
        HelpComponent,
        BookingComponent,
        BookingInfoComponent,
        BookingApprovalComponent,
        BookingApprovalHowComponent,
        BookingUpdateComponent,
        BookingAnotherPointComponent,
        BookingCancelComponent,
        InfoUpdateComponent,
        PasswordComponent,
        PhoneComponent,
        PrivacyComponent,
    ],
    imports: [
        UserIdleModule.forRoot({idle: 43200, timeout: 1, ping: 60}),
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot(routes, {useHash: true, onSameUrlNavigation: 'reload'}),
        NgxMaskModule.forRoot(options),
        NgxSimpleCountdownModule,
        BrowserModule,
        BrowserAnimationsModule,
        TableModule,
        FontAwesomeModule,
        ButtonModule,
        InputTextModule,
        DropdownModule,
        InputSwitchModule,
        ToolbarModule,
        CalendarModule,
        FullCalendarModule,
        TooltipModule,
        ToastModule,
        ConfirmDialogModule,
        ChartModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
        },
        TokenStorageService,
        AuthorizedGuard,
        UnauthorizedGuard,
        MessageService,
        ConfirmationService,
        AdminGuard,
        DatePipe,
        GoogleAnalyticsService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
