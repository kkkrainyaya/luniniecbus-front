import {User} from './user';

export class Bus {
  id: number;
  name: string;
  number: string;
  color: string;
  driver: User;
  isDeleted: boolean;
}
