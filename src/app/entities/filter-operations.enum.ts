export enum FilterOperations {
  equals = ':',
  notEquals = '!',
  lt = '<',
  gt = '>',
  gte = '>:',
  lte = '<:',
  contains = '~',
  notContains = '!~',
  startsWith = ':~',
  endsWith = '~:',
  isNot = '!~',
}
