export interface Theme {
  name: string;
  properties: any;
}

export const defaultTheme: Theme = {
  name: 'defaultTheme',
  properties: {
    '--banner-img-opacity': '1',
    '--banner-title-color': '#363636',
    '--form-label-color': '#363636',
    '--navbar-item-color': '#4a4a4a',
    '--navbar-item-hover-color': '#FFC100',
    '--navbar-item-focus-color': '#feae00',
    '--navbar-item-transition': '.2s ease-in-out 0s',
    '--navbar-item-hover-transform': 'scale(1.1, 1.1)',
    '--navbar-burger-color': '#4a4a4a',
    '--navbar-dropdown-color': '#fff'
  }
};

export const dark: Theme = {
  name: 'dark',
  properties: {
    '--banner-img-opacity': '0',
    '--banner-title-color': '#f6ebd8',
    '--dark-form-label-color': '#f6ebd8',
    '--dark-navbar-item-color': '#f6ebd8',
    '--dark-navbar-item-hover-color': '#FFC100',
    '--dark-navbar-item-focus-color': '#feae00',
    '--dark-navbar-item-transition': '.2s ease-in-out 0s',
    '--dark-navbar-item-hover-transform': 'scale(1.1, 1.1)',
    '--dark-navbar-burger-color': '#f6ebd8',
    '--dark-navbar-dropdown-color': '#2f2f2f'
  }
};
