import {User} from './user';
import {Route} from './route';
import {Point} from './point';

export class Passenger {

  public id: number;
  public user: User;
  public route: Route;
  public name: string;
  public departurePoint: Point;
  public arrivalPoint: Point;
  public numberOfPlaces: number;
  public comment: string;
  public confirmed: boolean;
  public dispatcherName: string;
  public creationDate: Date;

  constructor() {
  }

}
