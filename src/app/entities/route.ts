import {Bus} from './bus';
import {City} from './city';
import {RouteStatus} from './route-status.enum';
import {User} from './user';

export class Route {
  id: number;
  departureCity: City;
  arrivalCity: City;
  departureDateTime: Date;
  bus: Bus;
  driver: User;
  platformNumber: number;
  freePlaces: number;
  status: RouteStatus;
  isAdditional: boolean;
  notes: string;
  waybillNumber: string;
  firmName: string;
  firmPhone: string;
}
