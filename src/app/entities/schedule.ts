import {City} from './city';
import {WeekDay} from '@angular/common';

export class Schedule {
    id: number;
    time: string;
    departureCity: City;
    arrivalCity: City;
    weekday: WeekDay;
    startDate: Date;
    endDate: Date;
    platformNumber: number;
    isAdditional: boolean;
    firmId: number;
    firmName: string;
}
