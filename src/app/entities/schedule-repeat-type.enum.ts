export enum ScheduleRepeatType {
  EACH_WEEK = 'EACH_WEEK',
  ONE_TIME = 'ONE_TIME'
}
