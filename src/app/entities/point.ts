export class Point {
  id: number;
  name: string;
  time: string;
  cityId: number;
  adminOnly: boolean;
}
