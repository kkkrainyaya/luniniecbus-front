import {FilterMetadata} from 'primeng/api/filtermetadata';

export type TableFilter = {
  [s: string]: FilterMetadata;
};
