export abstract class RouteData {

  constructor(private readonly parent: RouteData,
              private readonly baseUrl: string,
  ) {
    this.parent = parent;
  }

  public getFullUrl(url: string): string[] {
    return [...this.concatBaseUrl(this), url];
  }

  private concatBaseUrl(route: RouteData): string[] {
    return route.parent ? [...this.concatBaseUrl(route.parent), route.baseUrl] : [route.baseUrl];
  }

}
