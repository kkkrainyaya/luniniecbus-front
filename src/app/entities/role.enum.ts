export enum Role {
  USER = 'USER',
  DISPATCHER = 'DISPATCHER',
  ADMIN = 'ADMIN',
  DRIVER = 'DRIVER',
  DRIVER_INACTIVE = 'DRIVER_INACTIVE',
}
