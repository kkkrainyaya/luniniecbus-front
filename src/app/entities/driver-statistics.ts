export class DriverStatistics {
  driverId: number;
  passengersCount: number;
  driverName: string;
}
