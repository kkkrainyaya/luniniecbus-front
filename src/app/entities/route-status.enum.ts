export enum RouteStatus {
  ACTIVE = 'ACTIVE',
  CANCELED = 'CANCELED',
}
