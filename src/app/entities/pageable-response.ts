export interface PageableResponse<T> {
  pageable: any;
  numberOfElements: number;
  content: T[];
  totalElements: number;
}
