export class Search {
  departureCityId: number;
  date: Date;

  constructor(departurePointId: number, date: Date) {
    this.departureCityId = departurePointId;
    this.date = date;
  }
}
