import {City} from './city';
import {WeekDay} from '@angular/common';

export class ScheduleMean {
  id: number;
  time: string;
  departureCity: City;
  weekday: WeekDay;
  passengersAverage: number;
}
