import {Point} from './point';

export class RoutePoints {
  landingPoints: Point[];
  dropOffPoints: Point[];
}
