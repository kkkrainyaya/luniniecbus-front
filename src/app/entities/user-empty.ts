import {User} from './user';

export class UserEmpty extends User {

  constructor() {
    super(null, '', '', [], null, null, null, null, null);
  }
}
