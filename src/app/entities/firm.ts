export class Firm {
    id: number;
    name: string;
    phone: string;
    isPhoneVisible: boolean;
}
