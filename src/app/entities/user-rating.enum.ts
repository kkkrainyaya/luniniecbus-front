export enum UserRating {
  GREEN = 'GREEN',
  YELLOW = 'YELLOW',
  RED = 'RED'
}
