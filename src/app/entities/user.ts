import {Role} from './role.enum';
import {UserRating} from './user-rating.enum';

export class User {

  constructor(
    public id: number,
    public name: string,
    public phone: string,
    public roles: Role[],
    public password: string,
    public rating: UserRating,
    public blockedTill: Date,
    public onRepairDates: Date[],
    public userId: string
  ) {
  }

}
