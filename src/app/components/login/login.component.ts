import {Component, OnInit} from '@angular/core';
import {Login} from '../../entities/login';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {User} from '../../entities/user';
import {MessageService} from 'primeng/api';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {HttpStatus} from '../../constants/http-status.enum';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private readonly PHONE_LENGTH = 9;
  private readonly RATE_LIMIT_REMAINING_HEADER = 'x-ratelimit-remaining';

  public login: Login = new Login();
  public isOtpSent = false;
  public user: User;
  public isOtpEnabled = true;
  public isPasswordEnabled = true;
  public otpCountdownEnd: number;
  public passwordCountdownEnd: number;
  public isOtpView = true;
  public showPassword = false;
  public userName: string;
  private rateLimitRemaining = 0;

  constructor(private userService: UserService,
              private router: Router,
              private messageService: MessageService,
              private analyticsService: GoogleAnalyticsService) {
  }

  public ngOnInit(): void {
    this.userService.getCurrentUserData().subscribe(data => this.user = data);
  }

  public sendOtp(): void {
    if (this.login.phone.length === this.PHONE_LENGTH) {
      this.userService.sendOtp(this.login.phone).pipe(
        tap((resp: HttpResponse<any>) => {
          this.isOtpSent = true;
          this.rateLimitRemaining = +resp.headers.get(this.RATE_LIMIT_REMAINING_HEADER) || 0;
          this.analyticsService.eventEmitter('otp_send', 'login');
        }),
        catchError((error: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('otp_send_error', error.status, error.message);
          if (error.status === HttpStatus.TooManyRequests) {
            this.isOtpSent = false;
            this.login.otp = null;
            this.isOtpEnabled = false;
            this.otpCountdownEnd = this.getCountdownEnd();
          }
          return of(error);
        })
      ).subscribe();
    }
  }

  public submitOtp(): void {
    if (!!this.login.otp) {
      this.userService.loginWithOtp(this.login)
        .pipe(
          tap((data: User) => {
            this.user = data;
            this.analyticsService.eventEmitter('login_via_otp', 'login');
            if (!!this.user.name && this.router.url === '/login') {
              this.router.navigate(['/']);
            }
          }),
          catchError((err: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('login_via_otp_error', err.status, err.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Неверный код'});
            this.isOtpSent = false;
            this.login.otp = null;
            if (this.rateLimitRemaining === 0) {
              this.isOtpEnabled = false;
              this.otpCountdownEnd = this.getCountdownEnd();
            }
            return of(err);
          })
        ).subscribe();
    }
  }

  public setName(): void {
    if (!!this.userName) {
      this.user.name = this.userName;
      this.userService.updateCurrentUser(this.user)
        .pipe(
          tap(() => {
            this.analyticsService.eventEmitter('name_added', 'login');
            this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Регистация прошла успешно'});
          }),
          catchError((response: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('name_adding_error', response.status, response.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось внести изменения'});
            return of(response);
          })
        ).subscribe(() => {
        if (this.router.url === '/login') {
          this.router.navigate(['/']);
        }
      });
    }
  }

  public resetOtpCountdown(): void {
    this.isOtpEnabled = true;
  }

  public resetPasswordCountdown(): void {
    this.isPasswordEnabled = true;
  }

  public getCountdownEnd(): number {
    const countdownEndDate = new Date();
    countdownEndDate.setMinutes(countdownEndDate.getMinutes() + 1);
    return countdownEndDate.getTime() / 1000;
  }

  public toggleOtpView(): void {
    this.isOtpView = !this.isOtpView;
  }

  public loginViaPassword(): void {
    if (!!this.login.password) {
      this.userService.loginWithPassword(this.login)
        .pipe(
          tap((data: User) => {
            this.user = data;
            this.analyticsService.eventEmitter('login_via_password', 'login');
            if (!!this.user.name && this.router.url === '/login') {
              this.router.navigate(['/']);
            }
          }),
          catchError((error: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('login_via_password_error', error.status, error.message);
            if (!!error.status && error.status === HttpStatus.NotFound) {
              this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Пароль не установлен'});
            } else if (error.status === HttpStatus.TooManyRequests) {
              this.isPasswordEnabled = false;
              this.passwordCountdownEnd = this.getCountdownEnd();
            } else {
              this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Неверный пароль'});
            }
            return of(error);
          })
        ).subscribe();
    }
  }

  public toggleShowPassword(): void {
    this.showPassword = !this.showPassword;
  }
}
