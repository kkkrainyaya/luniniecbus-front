import {Component, OnInit} from '@angular/core';
import {User} from '../../entities/user';
import {UserService} from '../../services/user.service';
import {Role} from '../../entities/role.enum';
import {UserEmpty} from '../../entities/user-empty';
import {NavigationEnd, Router} from '@angular/router';
import {ThemeService} from '../../services/theme.service';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    isMenuVisible = false;
    isDarkTheme = false;
    user: User;

    constructor(private userService: UserService,
                private router: Router,
                private themeService: ThemeService,
                private analyticsService: GoogleAnalyticsService) {
    }

    ngOnInit(): void {
        this.userService.currentUser$.subscribe(user => {
            this.user = user;
        });
        this.setTheme();
    }

    isDispatcher(user: User): boolean {
        return user && user.roles.includes(Role.DISPATCHER);
    }

    isAdmin(user: User): boolean {
        return user && user.roles.includes(Role.ADMIN);
    }

    toggleMenu(): void {
        this.isMenuVisible = !this.isMenuVisible;
    }

    onCallClicked(): void {
        this.analyticsService.eventEmitter('call_clicked', 'click');
        this.toggleMenu();
    }

    logout(): void {
        this.userService.clear();
    }

    userExists(): boolean {
        return !(this.user instanceof UserEmpty);
    }

    private setTheme(): void {
        if (this.themeService.getActiveTheme().name === 'dark') {
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    this.isDarkTheme = this.router.url === '/' || this.router.url === '/dispatcher;tab=search';
                }
            });
        }
    }

}
