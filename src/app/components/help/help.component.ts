import {Component, OnInit} from '@angular/core';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {

  constructor(public analyticsService: GoogleAnalyticsService) {
  }

  ngOnInit(): void {
    this.analyticsService.eventEmitter('help_page', 'page_view');
  }

}
