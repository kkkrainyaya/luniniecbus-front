import {Component, Input, OnInit} from '@angular/core';
import {Passenger} from '../../entities/passenger';
import {Route} from '../../entities/route';
import {PassengerService} from '../../services/passenger.service';
import {RouteService} from '../../services/route.service';
import {UserService} from '../../services/user.service';
import {User} from '../../entities/user';
import {RouteConstants} from '../../constants/route-constants';
import {Router} from '@angular/router';

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.css']
})
export class BookingFormComponent implements OnInit {
  passenger: Passenger;
  selectedRoute: Route;
  showBookingResult = false;
  user: User;

  constructor(private passengerService: PassengerService,
              private routeService: RouteService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
    });
  }

  @Input()
  set route(value: Route) {
    this.passenger = new Passenger();
    this.selectedRoute = value;
    this.passenger.route = this.selectedRoute;
  }

  createdPassenger(passenger: Passenger): void {
    this.passenger = passenger;
    this.showBookingResult = true;
  }

  cancelChanges(): void {
    // TODO: cancel
  }

  approveBookingResults(): void {
    this.showBookingResult = false;
    this.router.navigateByUrl(RouteConstants.PERSONAL.ORDERS);
  }
}
