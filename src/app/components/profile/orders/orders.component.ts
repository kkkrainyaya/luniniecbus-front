import {Component, OnInit} from '@angular/core';
import {Passenger} from '../../../entities/passenger';
import {PassengerService} from '../../../services/passenger.service';
import {UserService} from '../../../services/user.service';
import {User} from '../../../entities/user';
import {RouteService} from '../../../services/route.service';
import {PassengerCore} from '../../passenger-core/passenger-core';
import {of} from 'rxjs';
import {ConfirmationService, MessageService} from 'primeng/api';
import {Route} from '../../../entities/route';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css'],
})
export class OrdersComponent extends PassengerCore implements OnInit {

    public readonly firstPage = 0;
    public readonly pageSize = 10;

    public user: User;
    public selectedPassenger: Passenger;
    public isAllElements: boolean;
    public currentPageSize = this.pageSize;
    public selectedRoute: Route;
    public firmSurnameToPhone = new Map<string, string>(
        [['Кохович', '+375293375192'], ['Ракович', '+375447949259']]
    );

    constructor(protected passengerService: PassengerService,
                protected userService: UserService,
                protected routeService: RouteService,
                protected confirmationService: ConfirmationService,
                protected analyticsService: GoogleAnalyticsService,
                protected datePipe: DatePipe,
                private messageService: MessageService) {
        super(passengerService, userService, routeService, confirmationService, analyticsService, datePipe);
    }

    public ngOnInit(): void {
        this.userService.currentUser$.subscribe(data => this.user = data);
        this.loadPassengers();
        this.analyticsService.eventEmitter('orders_page', 'page_view');
    }

    public onRemovePassenger(response: any): any {
        this.loadPassengers();
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Заказ отменен'});
        return of(response);
    }

    public onConfirmPassenger(response: any): any {
        this.loadPassengers();
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Заказ подтвержден'});
        return of(response);
    }

    public refreshPassengers(): void {
        this.toggleEditDialog();
        this.selectedPassengerId = null;
        this.loadPassengers();
    }

    public loadMore(): void {
        this.currentPageSize += this.pageSize;
        this.loadPassengers();
    }

    public toggleActive(): void {
        this.onlyActive = !this.onlyActive;
        this.loadPassengers();
    }

    public cancelChanges(): void {
        this.toggleEditDialog();
        this.selectedPassengerId = null;
    }

    public showPassenger(passenger: Passenger): void {
        this.selectedPassengerId = passenger.id;
        this.selectedPassenger = passenger;
        this.passengerRouteId = passenger.route.id;
        this.routeService.getRouteById(this.selectedPassenger.route.id).subscribe(data => this.selectedRoute = data);
        this.toggleEditDialog();
    }

    public canModify(passenger: Passenger): boolean {
        return Math.abs((new Date(passenger.route.departureDateTime).getTime() - Date.now()) / 36e5) > 1;
    }

    public canConfirm(passenger: Passenger): boolean {
        const hoursBeforeDeparture = (new Date(passenger.route.departureDateTime).getTime() - Date.now()) / 36e5;
        return hoursBeforeDeparture > 1 && hoursBeforeDeparture < 24;
    }

    private loadPassengers(): void {
        this.passengerService.loadPassengers(
            this.currentPageSize,
            this.firstPage,
            'route.date',
            -1,
            null,
            null,
            this.user.id,
            this.onlyActive).subscribe(data => {
            this.passengers = data.content;
            this.isAllElements = data.totalElements !== 0 ? data.totalElements === data.content.length : true;
        });
    }

}
