import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../entities/user';
import {UserEmpty} from '../../../entities/user-empty';
import {MessageService} from 'primeng/api';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {catchError, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent implements OnInit {

  public user: User = new UserEmpty();
  public showPassword = false;
  public invalidPassword = false;
  private changedName: string;
  private changedPassword: string;
  private passwordMinLength = 8;

  constructor(private userService: UserService,
              private messageService: MessageService,
              private analyticsService: GoogleAnalyticsService) {
  }

  public ngOnInit(): void {
    this.userService.currentUser$.subscribe(
      data => this.user = data
    );
    this.analyticsService.eventEmitter('profile_page', 'page_view');
  }

  public update(): void {
    if (!!this.changedName) {
      this.user.name = this.changedName;
      this.userService.updateCurrentUser(this.user).pipe(
        tap((user) => {
          this.analyticsService.eventEmitter('profile_updated', 'user');
          this.user = user;
          this.showUpdatedMessage();
        }),
        catchError((response: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('profile_updating_error', response.status, response.message);
          this.showErrorMessage();
          return of(response);
        })
      ).subscribe();
    }
    if (!!this.changedPassword && this.changedPassword.length >= this.passwordMinLength) {
      this.userService.updatePassword(this.user, this.changedPassword).pipe(
        tap(() => {
          this.analyticsService.eventEmitter('password_updated', 'user');
          this.invalidPassword = false;
          this.showUpdatedMessage();
        }),
        catchError((response: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('password_updating_error', response.status, response.message);
          this.showErrorMessage();
          return of(response);
        })
      ).subscribe();
    } else if (!!this.changedPassword && this.changedPassword.length < this.passwordMinLength) {
      this.invalidPassword = true;
    }
  }

  public setName(value: string): void {
    this.changedName = value;
  }

  public setPassword(value: string): void {
    this.changedPassword = value;
  }

  public toggleShowPassword(): void {
    this.showPassword = !this.showPassword;
  }

  private showUpdatedMessage(): void {
    this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Данные обновлены'});
  }

  private showErrorMessage(): void {
    this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось сохранить изменения'});
  }

}
