import {Component, OnInit} from '@angular/core';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor(private analyticsService: GoogleAnalyticsService) {
  }

  ngOnInit(): void {
    this.analyticsService.eventEmitter('privacy_page', 'page_view');
  }

}
