import {Component, OnInit} from '@angular/core';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private analyticsService: GoogleAnalyticsService) {
  }

  ngOnInit(): void {
    this.analyticsService.eventEmitter('about_page', 'page_view');
  }

}
