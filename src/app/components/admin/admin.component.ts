import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {User} from '../../entities/user';
import {Role} from '../../entities/role.enum';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, AfterViewInit {

  public isRoutes = true;
  public isSchedule = false;
  public routeId: number;
  public user: User;
  public isAdmin = false;
  public isStaff = false;
  public isBuses = false;
  public isStaffSchedule = false;

  constructor(private route: ActivatedRoute,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
      this.isAdmin = user && user.roles.includes(Role.ADMIN);
    });
  }

  ngAfterViewInit(): void {
    this.fixHeader();
  }

  toggleRoutes(): void {
    if (!this.isRoutes) {
      this.isRoutes = !this.isRoutes;
      this.isSchedule = false;
      this.isStaff = false;
      this.isStaffSchedule = false;
      this.isBuses = false;
      this.routeId = null;
    }
  }

  toggleSchedule(): void {
    if (!this.isSchedule) {
      this.isRoutes = false;
      this.isStaff = false;
      this.isStaffSchedule = false;
      this.isBuses = false;
      this.isSchedule = !this.isSchedule;
      this.routeId = null;
    }
  }

  toggleDispatchers(): void {
    if (!this.isStaff) {
      this.isRoutes = false;
      this.isSchedule = false;
      this.isStaffSchedule = false;
      this.isBuses = false;
      this.isStaff = !this.isStaff;
      this.routeId = null;
    }
  }

  toggleBuses(): void {
    if (!this.isBuses) {
      this.isBuses = !this.isBuses;
      this.isRoutes = false;
      this.isStaffSchedule = false;
      this.isSchedule = false;
      this.isStaff = false;
      this.routeId = null;
    }
  }

  toggleStaffSchedule(): void {
    if (!this.isStaffSchedule) {
      this.isStaffSchedule = !this.isStaffSchedule;
      this.isBuses = false;
      this.isRoutes = false;
      this.isSchedule = false;
      this.isStaff = false;
      this.routeId = null;
    }
  }

  private fixHeader(): void {
    const header = document.getElementById('tabs');
    const sticky = header.offsetTop / 4;
    window.onscroll = () => {
      if (window.pageYOffset > sticky) {
        header.classList.add('sticky');
      } else {
        header.classList.remove('sticky');
      }
    };
  }

}
