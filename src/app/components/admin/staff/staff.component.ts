import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../entities/user';
import {Role} from '../../../entities/role.enum';
import {UserService} from '../../../services/user.service';
import {Table} from 'primeng/table';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {PageableResponse} from '../../../entities/pageable-response';
import {UserEmpty} from '../../../entities/user-empty';
import {Bus} from '../../../entities/bus';
import {BusService} from '../../../services/bus.service';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  @ViewChild(Table) public staffTable: Table;
  public selectedUser: User;
  public staff: User[] = [];
  public roles: Role[] = [];
  public isEditDialogActive = false;
  public loading = true;
  public lastLoadEvent: LazyLoadEvent;
  public totalRecords: number;
  public selectedBus: Bus;
  public buses: Bus[] = [];

  constructor(private userService: UserService,
              private messageService: MessageService,
              private busService: BusService,
              private confirmationService: ConfirmationService,
              private analyticsService: GoogleAnalyticsService) {
  }

  public ngOnInit(): void {
    this.roles = [Role.DISPATCHER, Role.ADMIN, Role.DRIVER, Role.USER];
  }

  public saveChanges(): void {
    if (!!this.selectedUser.id) {
      this.userService.update(this.selectedUser)
        .pipe(
          tap(data => {
            this.loadStaff(this.lastLoadEvent);
            this.updateDriver();
            this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Изменения сохранены'});
          }),
          catchError((response: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('staff_updating_error', response.status, response.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось сохранить изменения'});
            return of(response);
          })
        ).subscribe();
    } else {
      this.userService.addEmployee(this.selectedUser)
        .pipe(
          tap(data => {
            this.loadStaff(this.lastLoadEvent);
            this.updateDriver();
            this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Сотрудник добавлен'});
          }),
          catchError((response) => {
            this.analyticsService.errorEmitter('staff_creation_error', response.status, response.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось добавить сотрудника'});
            return of(response);
          })
        ).subscribe();
    }
    this.toggleEditDialog();
  }

  public toggleEditDialog(): void {
    this.isEditDialogActive = !this.isEditDialogActive;
  }

  public edit(dispatcher: User): void {
    this.selectedUser = dispatcher;
    if (this.isDriver(this.selectedUser)) {
      this.selectedBus = this.buses.find(it => it.driver?.id === this.selectedUser.id);
    }
    this.toggleEditDialog();
  }

  public loadStaff(event: LazyLoadEvent): void {
    this.loading = true;
    this.userService.loadAll(
      event.rows,
      event.first / event.rows,
      event.sortField || 'name',
      event.sortOrder,
      true,
    ).subscribe((resp: PageableResponse<User>) => {
      this.lastLoadEvent = event;
      this.lastLoadEvent.first = 0;
      this.staff = resp.content;
      this.loading = false;
      this.totalRecords = resp.totalElements;
      this.busService.getBuses().subscribe((data: Bus[]) => this.buses = data.filter(it => !it.isDeleted));
    });
  }

  public addNewStaff(): void {
    this.selectedUser = new UserEmpty();
    this.toggleEditDialog();
  }

  public getBus(driverId: number): string {
    const bus = this.buses.find(it => it.driver?.id === driverId);
    return !!bus ? bus.name + ' ' + bus.number : '';
  }

  private updateDriver(): void {
    if (this.isDriver(this.selectedUser) && !!this.selectedBus) {
      this.busService.updateDriver(this.selectedBus.id, this.selectedUser.id)
        .pipe(
          tap((data) => {
            this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Водитель обновлен'});
          }),
          catchError((response) => {
            this.analyticsService.errorEmitter('driver_updating_error', response.status, response.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось изменить водителя'});
            return of(response);
          })).subscribe();
    }
  }

  public setUserRole(employee: User): void {
    this.confirmationService.confirm({
      message: 'Хотите удалить сотрудника? (Ему будет назначена роль USER)',
      accept: () => {
        const roles = employee.roles.find(it => it === Role.DRIVER) ? [Role.USER, Role.DRIVER_INACTIVE] : [Role.USER];
        this.userService.updateRole(employee, roles)
          .pipe(
            tap(() => this.loadStaff(this.lastLoadEvent)),
            catchError((response) => {
              this.analyticsService.errorEmitter('staff_deletion_error', response.status, response.message);
              this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось удалить сотрудника'});
              return of(response);
            })
          ).subscribe();
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Сотрудник удален'});
      },
      reject: () => {
      }
    });

  }

  public isDriver(user: User): boolean {
    return user.roles.includes(Role.DRIVER);
  }

  public getExistedUser(): void {
    this.userService.getByPhone(this.selectedUser.phone).subscribe(data => {
      this.selectedUser.name = data.name;
      this.selectedUser.roles = data.roles;
    }, () => {
      this.selectedUser.name = null;
    });
  }
}
