import {Component, OnInit, ViewChild} from '@angular/core';
import {Route} from '../../../entities/route';
import {Table} from 'primeng/table';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {RouteService} from '../../../services/route.service';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';
import {PageableResponse} from '../../../entities/pageable-response';
import {RouteStatus} from '../../../entities/route-status.enum';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {catchError, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {

  @ViewChild(Table) public routesTable: Table;
  public routes: Route[] = [];
  public loading = true;
  public cities: string[] = [];
  public lastLoadEvent: LazyLoadEvent;
  public filters = new Map<string, string>();
  public defaultSortFields = new Map<string, number>([['date', 0], ['schedule.time', 1]]);
  public onlyActive = false;
  public totalRecords: number;

  constructor(public routeService: RouteService,
              public userService: UserService,
              public router: Router,
              private confirmationService: ConfirmationService,
              private messageService: MessageService,
              private analyticsService: GoogleAnalyticsService) {
  }

  public ngOnInit(): void {
    this.getCities();
  }

  public isActive(route: Route): boolean {
    return new Date(route.departureDateTime).getTime() > new Date().getTime();
  }

  public loadRoutes(event: LazyLoadEvent): void {
    this.loading = true;
    this.routeService.loadRoutes(
      event.rows,
      event.first / event.rows,
      this.getSortFields(event),
      this.filters,
      this.onlyActive,
    ).subscribe((resp: PageableResponse<Route>) => {
      this.lastLoadEvent = event;
      this.lastLoadEvent.first = 0;
      this.routes = resp.content;
      this.loading = false;
      this.totalRecords = resp.totalElements;
    });
  }

  public filter(field: string, value: string): void {
    if (!!value) {
      if (field === 'date') {
        value = new Date(value).toLocaleDateString();
      }
      this.filters.set(field, value);
    } else {
      this.filters.delete(field);
    }
    this.loadRoutes(this.lastLoadEvent);
  }

  public clear(): void {
    this.filters.clear();
    this.loadRoutes(this.lastLoadEvent);
  }

  public showPassengers(routeId: number): void {
    this.router.navigate(['dispatcher', {tab: 'passengers', routeId}]);
  }

  public cancelRoute(route: Route): void {
    this.confirmationService.confirm({
      message: 'Хотите отменить рейс?',
      accept: () => {
        route.status = RouteStatus.CANCELED;
        this.update(route);
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Рейс отменён'});
      },
      reject: () => {
      }
    });
  }

  public activateRoute(route: Route): void {
    this.confirmationService.confirm({
      message: 'Хотите возобновить рейс?',
      accept: () => {
        route.status = RouteStatus.ACTIVE;
        this.update(route);
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Рейс возобновлён'});
      },
      reject: () => {
      }
    });
  }

  private getSortFields(event: LazyLoadEvent): Map<string, number> {
    return !!event.sortField && !!event.sortOrder
      ? new Map<string, number>([[event.sortField, event.sortOrder]])
      : this.defaultSortFields;
  }

  private update(route: Route): void {
    this.routeService.update(route)
      .pipe(
        tap((data) => {
          this.loadRoutes(this.lastLoadEvent);
        }),
        catchError((error: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('route_updating_error', error.status, error.message);
          return of(error);
        })
      ).subscribe();
  }

  private getCities(): void {
    this.routeService.getCities().subscribe(data => {
      data.forEach(it => {
        this.cities.push(it.name);
      });
    });
  }
}
