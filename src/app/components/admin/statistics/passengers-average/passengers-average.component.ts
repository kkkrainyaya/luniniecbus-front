import {Component, OnInit, ViewChild} from '@angular/core';
import {ScheduleService} from '../../../../services/schedule.service';
import {ScheduleMean} from '../../../../entities/schedule-mean';
import {tap} from 'rxjs/operators';
import {PassengerService} from '../../../../services/passenger.service';
import {BusService} from '../../../../services/bus.service';
import {DriverStatistics} from '../../../../entities/driver-statistics';
import {UIChart} from 'primeng/chart';

@Component({
  selector: 'app-passengers-average',
  templateUrl: './passengers-average.component.html',
  styleUrls: ['./passengers-average.component.css'],
})
export class PassengersAverageComponent implements OnInit {

  @ViewChild('chartDeparture') chartDeparture: UIChart;
  @ViewChild('chartArrival') chartArrival: UIChart;
  @ViewChild('chartCountByCreationDate') chartCountByCreationDate: UIChart;
  @ViewChild('chartCountByRouteDate') chartCountByRouteDate: UIChart;

  public statisticsStart: Date;
  public statisticsEnd: Date;
  public scheduleMean: ScheduleMean[] = [];
  public departureData: any;
  public arrivalData: any;
  public basicOptions: any;
  public isDepartureData = true;
  public passengersCountDataByRouteDate: any;
  public passengersCountDataByCreationDate: any;
  public chartOptions: any;
  public driversStatistics: DriverStatistics[] = [];
  public selectedDatesByRouteDateStat: Date[];
  public selectedDatesByCreationDateStat: Date[];

  constructor(
    private scheduleService: ScheduleService,
    private passengerService: PassengerService,
    private busService: BusService,
  ) {
  }

  public ngOnInit(): void {
    setTimeout(() => {
      this.chartDeparture.reinit();
      this.chartCountByRouteDate.reinit();
      this.chartCountByCreationDate.reinit();
    }, 100);
    const date = new Date();
    this.statisticsStart = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    this.statisticsEnd = new Date(date.getFullYear(), date.getMonth(), 0);
    this.setChartsData();
    this.setOptions();
    this.loadPassengersMean();
    this.loadStatisticsByRouteDate();
    this.loadStatisticsByCreationDate();
    this.loadDriverStatistics();
  }

  public toggleData(): void {
    this.isDepartureData = !this.isDepartureData;
  }

  public loadStatisticsByRouteDate(): void {
    this.passengerService.getStatisticsByRouteDate(
      !!this.selectedDatesByRouteDateStat ? this.selectedDatesByRouteDateStat[0] : this.statisticsStart,
      !!this.selectedDatesByRouteDateStat ? this.selectedDatesByRouteDateStat[1] : this.statisticsEnd)
      .pipe(tap(data => {
        this.passengersCountDataByRouteDate.datasets[0].data = [];
        this.passengersCountDataByRouteDate.datasets[0].data.push(data.bookedByUserNum);
        this.passengersCountDataByRouteDate.datasets[0].data.push(data.bookedByDispatcherNum);
        this.chartCountByRouteDate.reinit();
      })).subscribe();
  }

  public loadStatisticsByCreationDate(): void {
    this.passengerService.getStatisticsByCreationDate(
      !!this.selectedDatesByCreationDateStat ? this.selectedDatesByCreationDateStat[0] : this.statisticsStart,
      !!this.selectedDatesByCreationDateStat ? this.selectedDatesByCreationDateStat[1] : this.statisticsEnd)
      .pipe(tap(data => {
        this.passengersCountDataByCreationDate.datasets[0].data = [];
        this.passengersCountDataByCreationDate.datasets[0].data.push(data.bookedByUserNum);
        this.passengersCountDataByCreationDate.datasets[0].data.push(data.bookedByDispatcherNum);
        this.chartCountByCreationDate.reinit();
      })).subscribe();
  }

  private setChartsData(): void {
    this.departureData = {
      labels: [],
      datasets: [
        {
          label: 'Лунинец',
          data: [],
          fill: false,
          borderColor: '#FFA726',
          tension: .4,
        },
      ]
    };
    this.arrivalData = {
      labels: [],
      datasets: [
        {
          label: 'Минск',
          data: [],
          fill: false,
          borderColor: '#42A5F5',
          tension: .4,
        },
      ]
    };
    this.passengersCountDataByRouteDate = {
      labels: ['Запись онлайн', 'Запись через диспетчера'],
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#FF6384',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#FFCE56'
          ]
        }
      ]
    };
    this.passengersCountDataByCreationDate = {
      labels: ['Запись онлайн', 'Запись через диспетчера'],
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#FF6384',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#FFCE56'
          ]
        }
      ]
    };
  }

  private loadPassengersMean(): void {
    this.scheduleService.getPassengersAverage(this.statisticsStart, this.statisticsEnd)
      .pipe(tap(data => {
        this.scheduleMean = data;
        this.scheduleMean.forEach(it => {
          if (it.departureCity.id === 1) {
            this.departureData.datasets[0].data.push(it.passengersAverage);
            this.departureData.labels.push(it.weekday + ' ' + it.time);
          } else {
            this.arrivalData.datasets[0].data.push(it.passengersAverage);
            this.arrivalData.labels.push(it.weekday + ' ' + it.time);
          }
        });
      })).subscribe();
  }

  private setOptions(): void {
    this.basicOptions = {
      plugins: {
        legend: {
          labels: {
            color: '#495057'
          }
        }
      },
      scales: {
        x: {
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          },
          beginAtZero: true
        },
        y: {
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          }
        }
      }
    };
    this.chartOptions = {
      legend: {
        position: 'right'
      }
    };
  }

  private loadDriverStatistics(): void {
    this.busService.getStatistics(this.statisticsStart, this.statisticsEnd).pipe(tap(data => {
      this.driversStatistics = data;
    })).subscribe();
  }
}
