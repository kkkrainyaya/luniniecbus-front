import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {RouteService} from '../../../services/route.service';
import {UserService} from '../../../services/user.service';
import {DatePipe} from '@angular/common';
import {Route} from '../../../entities/route';
import {MessageService} from 'primeng/api';
import {catchError, mergeMap, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import interactionPlugin from '@fullcalendar/interaction';
import {User} from '../../../entities/user';
import {Role} from '../../../entities/role.enum';

@Component({
    selector: 'app-staff-schedule',
    templateUrl: './staff-schedule.component.html',
    styleUrls: ['./staff-schedule.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StaffScheduleComponent implements OnInit, AfterViewInit {

    @ViewChild('calendar') calendarComponent: FullCalendarComponent;
    public readonly DEPARTURE_CITY_NAME = 'Лунинец';
    public readonly ARRIVAL_CITY_NAME = 'Минск';

    public options: any;
    public drivers: User[] = [];
    public isEditDialogActive = false;
    public selectedDate: Date;
    public selectedDriverId: number;
    public selectedDepartureRouteId: string;
    public selectedArrivalRouteId: string;
    public isSelectedOnRepair = false;
    private calendarApi;
    private startDate: Date;
    private endDate: Date;
    private routesByDate = new Map<string, Route[]>();
    private routes: Route[] = [];

    constructor(private routeService: RouteService,
                private userService: UserService,
                private datePipe: DatePipe,
                private messageService: MessageService,
                private analyticsService: GoogleAnalyticsService,
                private cdr: ChangeDetectorRef) {
    }

    public ngOnInit(): void {
        this.setCalendarOptions();
        this.cdr.detectChanges();
    }

    public ngAfterViewInit(): void {
        this.calendarApi = this.calendarComponent.getApi();
        this.updateCalendarPeriod();
        this.reloadCalendar();
        this.cdr.detectChanges();
    }

    public reloadCalendar(): void {
        this.updateCalendarPeriod();
        this.calendarApi.view.calendar.removeAllEvents();
        this.loadData();
    }

    public toggleEditDialog(): void {
        this.isEditDialogActive = !this.isEditDialogActive;
        if (!this.isEditDialogActive) {
            this.selectedDate = null;
            this.selectedDriverId = null;
            this.selectedDepartureRouteId = '';
            this.selectedArrivalRouteId = '';
            this.isSelectedOnRepair = false;
        }
    }

    public getDriverName(driverId: number): string {
        return this.drivers.find(it => it.id === driverId).name;
    }

    public getRoutes(date: Date, departureCityName: string): Route[] {
        return this.routesByDate.get(this.datePipe.transform(date, 'dd.MM.yy'))
            ?.filter(it => it.departureCity.name === departureCityName);
    }

    public updateDepartureRouteDriver(routeId: number, driverId: number): void {
        const previousRouteId = !!this.selectedDepartureRouteId ? this.selectedDepartureRouteId : null;
        this.updateDriverSchedule(previousRouteId, routeId, driverId);

    }

    public toggleOnRepair(): void {
        this.isSelectedOnRepair = !this.isSelectedOnRepair;
        this.updateOnRepairStatus(this.selectedDriverId, this.isSelectedOnRepair, this.selectedDate);

    }

    public updateArrivalRouteDriver(routeId: any, driverId: number): void {
        const previousRouteId = !!this.selectedArrivalRouteId ? this.selectedArrivalRouteId : null;
        this.updateDriverSchedule(previousRouteId, routeId, driverId);
    }

    private updateCalendarPeriod(): void {
        this.startDate = this.calendarApi.view.currentStart;
        this.endDate = this.calendarApi.view.currentEnd;
        this.endDate.setDate(this.endDate.getDate() - 1);
    }

    private updateDriverSchedule(previousRouteId: string, routeId: number, driverId: number): void {
        if (!!previousRouteId) {
            this.routeService.updateDriver(+previousRouteId, null)
                .pipe(
                    switchMap((data) => {
                        this.updateDriver(routeId, driverId);
                        return of(data);
                    })
                ).subscribe();
        } else {
            this.updateDriver(routeId, driverId);
        }
    }

    private updateOnRepairStatus(driverId: number, isOnRepair: boolean, selectedDate: Date): void {
        if (!!driverId && !!selectedDate) {
            this.userService.updateOnRepairStatus(driverId, isOnRepair, selectedDate)
                .pipe(
                    tap((data) => {
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Готово',
                            detail: 'Ремонтный день обновлен'
                        });
                        return of(data);
                    }),
                    catchError((response) => {
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Ошибка',
                            detail: 'Не удалось добавить ремонтный день'
                        });
                        return of();
                    })
                ).subscribe(() => this.reloadCalendar());
        }
    }

    private loadResources(): void {
        this.userService.getDrivers().subscribe(data => {
            this.drivers = data;
            const inactiveDrivers = data.filter(it => it.roles.includes(Role.DRIVER_INACTIVE));
            if (this.calendarApi.view.calendar.getResources().length > 0) {
                this.calendarApi.view.calendar.getResources().forEach(it => {
                    it.remove();
                });
            }

            this.drivers.filter(it => !it.roles.includes(Role.DRIVER_INACTIVE))
                .forEach(driver => {
                    this.addResource(driver);
                });
            inactiveDrivers.filter(it => !!this.routes.find(route => !!route.driver && route.driver.id === it.id))
                .forEach(driver => {
                    this.addResource(driver);
                });
            const driverOnRepairDate = this.drivers.filter(it => it.onRepairDates.length > 0)
                .flatMap(it => it.onRepairDates.map(date => [it.id, date]));
            driverOnRepairDate.forEach((idDate) =>
                this.calendarApi.view.calendar.addEvent({
                        id: idDate[0],
                        allDay: true,
                        color: 'orange',
                        textColor: 'black',
                        resourceId: idDate[0],
                        date: idDate[1],
                        title: 'PEM',
                        extendedProps: {
                            onRepairDate: idDate[1],
                            isOnRepair: true,
                            driverId: idDate[0]
                        },
                    }
                ));
        });
    }

    private addResource(driver: User): void {
        const folderStartIndex = driver.name.indexOf('(');
        const driverFolder = folderStartIndex !== -1 ? driver.name.substring(folderStartIndex + 1, driver.name.indexOf(')')) : 'ИП Крайний';
        this.calendarApi.view.calendar.addResource({
            id: driver.id,
            name: driver.name,
            sum: this.setRoutesSum(driver.id).toString(10),
            driver: driverFolder
        });
    }

    private updateDriver(routeId: number, driverId: number): void {
        this.routeService.updateDriver(routeId, driverId)
            .pipe(
                tap((data) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Готово',
                        detail: 'Расписание водителя успешно обновлено'
                    });
                    return of(data);
                }),
                catchError((response) => {
                    this.analyticsService.errorEmitter('driver_schedule_updating_error', response.status, response.message);
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Ошибка',
                        detail: 'Не удалось обновить расписание водителя'
                    });
                    return of();
                })
            ).subscribe(() => this.reloadCalendar());
    }

    private setCalendarOptions(): void {
        this.options = {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: '',
            },
            resourceGroupField: 'driver',
            selectable: true,
            locale: 'ru',
            initialView: window.innerWidth < 765 ? 'resourceTimelineDay' : 'resourceTimelineMonth',
            slotLaneDidMount: this.setDaysStyle(),
            resourceAreaColumns: [
                {
                    field: 'name',
                    headerContent: 'Имя'
                },
                {
                    field: 'sum',
                    headerContent: 'Итого рейсов'
                }
            ],
            firstDay: 1,
            nowIndicator: true,
            events: {},
            plugins: [
                resourceTimelinePlugin,
                interactionPlugin,
            ],
            dateClick: (info) => {
                this.selectedDate = info.date;
                this.selectedDriverId = +info.resource.id;
                this.selectedDepartureRouteId = '';
                this.selectedArrivalRouteId = '';
                this.isSelectedOnRepair = false;
                this.toggleEditDialog();
                this.cdr.detectChanges();
            },
            eventClick: (info) => {
                this.selectedDate = info.event.start;
                this.selectedDriverId = info.event.extendedProps.driverId;
                this.selectedDepartureRouteId = !!info.event.extendedProps.departureRoute ? info.event.extendedProps.departureRoute.id : '';
                this.selectedArrivalRouteId = !!info.event.extendedProps.arrivalRoute ? info.event.extendedProps.arrivalRoute.id : '';
                this.isSelectedOnRepair = !!info.event.extendedProps.isOnRepair;
                this.toggleEditDialog();
                this.cdr.detectChanges();
            },
            customButtons: {
                next: {
                    click: this.nextMonth.bind(this),
                },
                prev: {
                    click: this.prevMonth.bind(this),
                },
                today: {
                    text: 'сегодня',
                    click: this.todayMonth.bind(this),
                },
            },
        };
    }

    private setDaysStyle(): any {
        return (arg) => {
            if ([0, 6].includes(arg.date.getDay())) {
                arg.el.style.backgroundColor = 'rgb(255, 228, 225, 0.5)';
            }
        };
    }

    private nextMonth(): void {
        this.calendarApi.next();
        this.reloadCalendar();
    }

    private prevMonth(): void {
        this.calendarApi.prev();
        this.reloadCalendar();
    }

    private todayMonth(): void {
        this.calendarApi.today();
        this.reloadCalendar();
    }

    private loadData(): void {
        this.routeService.getAllByDateRange(this.startDate, this.endDate).pipe(
            tap(data => {
                this.routes = data;
                this.routes.forEach(it => {
                    const date = this.datePipe.transform(it.departureDateTime, 'dd.MM.yy');
                    const values = this.routesByDate.get(date) || [];
                    if (!values.find(route => it.id === route.id)) {
                        values.push(it);
                    }
                    this.routesByDate.set(date, values);
                });
                this.initEvents();
            }),
            mergeMap((data) => {
                this.loadResources();
                return of(data);
            })
        ).subscribe();
    }

    private initEvents(): void {
        const dateDriverRoutes = this.groupBy(this.routes.filter(it => !!it.driver),
            item => [item.driver.id, this.datePipe.transform(item.departureDateTime, 'dd.MM.yy')]);
        dateDriverRoutes.forEach(routes => {
            const departureRoute = routes.find(it => it.departureCity.name === this.DEPARTURE_CITY_NAME);
            const arrivalRoute = routes.find(it => it.departureCity.name === this.ARRIVAL_CITY_NAME);
            this.calendarApi.view.calendar.addEvent({
                    id: departureRoute?.id || arrivalRoute?.id,
                    allDay: true,
                    color: !!departureRoute ? 'paleturquoise' : 'lightgreen',
                    textColor: 'black',
                    resourceId: departureRoute?.driver.id || arrivalRoute?.driver.id,
                    date: departureRoute?.departureDateTime || arrivalRoute?.departureDateTime,
                    title: this.datePipe.transform(departureRoute?.departureDateTime || arrivalRoute?.departureDateTime, 'HH:mm'),
                    extendedProps: {
                        departureRoute,
                        arrivalRoute,
                        driverId: departureRoute?.driver.id || arrivalRoute?.driver.id
                    },
                }
            );
        });
    }

    private groupBy(array: any[], f): any {
        const groups = {};
        array.forEach(o => {
            const group = JSON.stringify(f(o));
            groups[group] = groups[group] || [];
            groups[group].push(o);
        });
        return Object.keys(groups).map(group => groups[group]);
    }

    private setRoutesSum(driverId: number): number {
        return this.calendarApi.view.calendar.getEvents()
            .filter(event => event.extendedProps.driverId === driverId && !!event.extendedProps.departureRoute).length;
    }

}
