import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {RouteService} from '../../../services/route.service';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {catchError, tap} from 'rxjs/operators';
import {ScheduleService} from '../../../services/schedule.service';
import {WeekDay} from '@angular/common';
import {Schedule} from '../../../entities/schedule';
import {DateUtility} from '../../../helpers/date-utility';
import {City} from '../../../entities/city';
import {ScheduleRepeatType} from '../../../entities/schedule-repeat-type.enum';
import {MessageService} from 'primeng/api';
import {Router} from '@angular/router';
import {of} from 'rxjs';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {HttpErrorResponse} from '@angular/common/http';
import {EventInput} from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import {Firm} from '../../../entities/firm';

@Component({
  selector: 'app-schedule',
  templateUrl: './admin-schedule.component.html',
  styleUrls: ['./admin-schedule.component.css']
})
export class AdminScheduleComponent implements OnInit, AfterViewInit {

  public readonly EACH_WEEK = ScheduleRepeatType.EACH_WEEK;
  public readonly ONE_TIME = ScheduleRepeatType.ONE_TIME;

  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  public showSchedule = false;
  public newRouteMenu = false;
  public newRouteDepartureCityId: string;
  public repeatType = this.EACH_WEEK;
  public newRouteDateTime: Date;
  public options: any;
  public cities: City[];
  public isEditDialogActive = false;
  public selectedEvent: EventInput;
  private startDate: Date;
  private endDate: Date;
  private calendarApi;
  private schedules: Schedule[] = [];
  private firms: Firm[] = [];

  private static getDateWithTime(currentDate: Date, schedule: Schedule): Date {
    const time = schedule.time.split(':', 2);
    const date = new Date(currentDate);
    date.setHours(Number.parseInt(time[0], 10), Number.parseInt(time[1], 10));
    return date;
  }

  constructor(private routeService: RouteService,
              private scheduleService: ScheduleService,
              private messageService: MessageService,
              private router: Router,
              private analyticsService: GoogleAnalyticsService) {
  }

  public ngOnInit(): void {
    this.setCalendarOptions();
    this.routeService.getCities().subscribe(
      data => this.cities = data
    );
    this.scheduleService.getFirms().subscribe(
        data => this.firms = data
    );
  }

  public ngAfterViewInit(): void {
    this.calendarApi = this.calendarComponent.getApi();
    this.startDate = this.calendarApi.view.currentStart;
    this.endDate = this.calendarApi.view.currentEnd;
    this.loadEvents();
  }

  public toggleSchedule(): void {
    this.showSchedule = !this.showSchedule;
    this.loadEvents();
  }

  public loadEvents(): void {
    this.startDate = this.calendarApi.view.currentStart;
    this.endDate = this.calendarApi.view.currentEnd;
    this.calendarApi.view.calendar.removeAllEvents();
    if (this.showSchedule) {
      this.loadSchedules();
    } else {
      this.loadRoutes();
    }
  }

  public getFormattedTime(time: any): string {
    return ('0' + time.getHours()).slice(-2) + ':' + ('0' + time.getMinutes()).slice(-2);
  }

  public onNewRouteMenuShow(): void {
    this.toggleNewRouteMenu();
    this.calendarApi.view.calendar.updateSize();
  }


  public toggleNewRouteMenu(): void {
    this.newRouteMenu = !this.newRouteMenu;
    if (!this.newRouteMenu) {
      this.repeatType = null;
      this.newRouteDateTime = null;
      this.newRouteDepartureCityId = null;
    }
  }

  public addNewRoute(): void {
    const schedule = new Schedule();
    schedule.startDate = this.newRouteDateTime;
    schedule.time = this.newRouteDateTime.toTimeString().substring(0, 5);
    schedule.departureCity = new City(+this.newRouteDepartureCityId);
    schedule.arrivalCity = this.cities.find(it => it.id !== +this.newRouteDepartureCityId);
    schedule.endDate = this.repeatType === ScheduleRepeatType.ONE_TIME ? schedule.startDate : null;
    this.scheduleService.create(schedule)
      .pipe(
        tap((data) => {
          this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Рейс добавлен'});
          this.toggleNewRouteMenu();
          this.loadEvents();
        }),
        catchError((response: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('route_creation_error', response.status, response.message);
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось добавить рейс'});
          return of(response);

        })
      ).subscribe();
  }

  public toggleEditDialog(): void {
    this.isEditDialogActive = !this.isEditDialogActive;
    this.loadEvents();
  }

  public updateSchedule(id: string, endDate: any): void {
    const schedule = this.schedules.find(it => it.id.toString(10) === id);
    schedule.endDate = endDate;
    schedule.platformNumber = this.selectedEvent.extendedProps.schedule.platformNumber;
    schedule.firmId = this.selectedEvent.extendedProps.schedule.firmId;
    this.scheduleService.update(schedule).pipe(
      tap(data => {
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Расписание обновлено'});
        this.selectedEvent = null;
        this.toggleEditDialog();
        this.loadEvents();
      }),
      catchError((err: HttpErrorResponse) => {
        this.analyticsService.errorEmitter('schedule_updating_error', err.status, err.message);
        this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось обновить расписание'});
        return of(err);
      })
    ).subscribe();
  }

  private setCalendarOptions(): void {
    this.options = {
      locale: 'ru',
      initialView: window.innerWidth < 765 ? 'listDay' : 'timeGridWeek',
      headerToolbar: {
        left: 'prev,next,today',
        center: 'title',
        right: 'timeGridWeek,timeGridDay'
      },
      plugins: [
          timeGridPlugin,
      ],
      allDaySlot: false,
      dayCellDidMount: this.setDaysStyle(),
      eventContent: this.setEventContent(),
      editable: true,
      selectable: true,
      selectMirror: true,
      dayMaxEvents: true,
      nowIndicator: true,
      droppable: true,
      loading: this.loadEvents.bind(this),
      firstDay: 1,
      buttonText: {
        today: 'сегодня',
        week: 'неделя',
        day: 'день'
      },
      events: {},
      eventClick: (info) => {
        if (!!info.event.id) {
          this.selectedEvent = info.event;
          this.toggleEditDialog();
        } else {
          this.router.navigate(['dispatcher', {tab: 'passengers', routeId: info.event.extendedProps.routeId}]);
        }
      },
      customButtons: {
        next: {
          click: this.nextWeek.bind(this),
        },
        prev: {
          click: this.prevWeek.bind(this),
        },
        today: {
          text: 'сегодня',
          click: this.todayWeek.bind(this),
        },
      },
    };
  }

  private setEventContent(): any {
    return (arg) => {
      const element = document.createElement('p');
      if (arg.event.extendedProps.isAdditional) {
        const repeat = document.createElement('span');
        repeat.style.color = '';
        repeat.style.fontSize = 'large';
        repeat.style.fontWeight = 'bold';
        repeat.innerHTML = ' <span class="tag is-link">доп</span>';
        element.appendChild(repeat);
      }
      const time = document.createElement('span');
      time.style.fontWeight = 'bold';
      time.innerHTML = this.getFormattedTime(arg.event.start);

      element.appendChild(time);
      const title = document.createElement('span');
      title.innerHTML = ' ' + arg.event.title;

      element.appendChild(title);

      const arrayOfDomNodes = [element];
      return {domNodes: arrayOfDomNodes};
    };
  }

  private setDaysStyle(): any {
    return (arg) => {
      if ([0, 6].includes(arg.date.getDay())) {
        arg.el.style.backgroundColor = 'mistyrose';
      }
    };
  }

  private loadSchedules(): void {
    this.scheduleService.getAll(this.startDate, this.endDate)
      .pipe(tap(data => {
        this.schedules = data;
        data.forEach(schedule => {
          const start = this.getStart(schedule);
          if (!!start) {
            this.calendarApi.view.calendar.addEvent({
              id: schedule.id,
              start,
              title: schedule.departureCity.name,
              backgroundColor: schedule.departureCity.id === 1 ? 'rgb(135, 206, 250, 0.6)' : 'rgb(119, 136, 153, 0.6)',
              textColor: schedule.departureCity.id === 1 ? 'black' : 'white',
              extendedProps: {isAdditional: schedule.isAdditional, schedule},
            });
          }
        });
      })).subscribe();
  }

  private loadRoutes(): void {
    this.routeService.getAllByDateRange(this.startDate, this.endDate)
      .pipe(tap(data => {
        data.forEach(route => this.calendarApi.view.calendar.addEvent({
          start: route.departureDateTime,
          title: route.departureCity.name,
          backgroundColor: route.departureCity.id === 1 ? 'rgb(124, 252, 0, 0.5)' : 'rgb(255, 255, 0, 0.5)',
          textColor: 'black',
          droppable: false,
          editable: false,
          extendedProps: {
            isAdditional: route.isAdditional,
            routeId: route.id,
          },
        }));
      })).subscribe();
  }

  private getStart(schedule: Schedule): Date {
    const datesRange = DateUtility.getDatesInRange(this.startDate, this.endDate);
    const date = datesRange.find(it => schedule.weekday.toString(10) === WeekDay[it.getDay()].toUpperCase());
    return !!date && (schedule.endDate === null || new Date(schedule.endDate) >= date)
      ? AdminScheduleComponent.getDateWithTime(date, schedule) : null;
  }

  private nextWeek(): void {
    this.calendarApi.next();
    this.loadEvents();
  }

  private prevWeek(): void {
    this.calendarApi.prev();
    this.loadEvents();
  }

  private todayWeek(): void {
    this.calendarApi.today();
    this.loadEvents();
  }

}
