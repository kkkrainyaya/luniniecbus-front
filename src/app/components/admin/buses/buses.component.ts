import {Component} from '@angular/core';
import {Bus} from '../../../entities/bus';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {BusService} from '../../../services/bus.service';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {PageableResponse} from '../../../entities/pageable-response';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-buses',
  templateUrl: './buses.component.html',
  styleUrls: ['./buses.component.css']
})
export class BusesComponent {

  public selectedBus: Bus;
  public isEditDialogActive = false;
  public loading = true;
  public buses: Bus[] = [];
  public totalRecords: number;

  private lastLoadEvent: LazyLoadEvent;

  constructor(private busService: BusService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService,
              private analyticsService: GoogleAnalyticsService) {
  }

  public addNewBus(): void {
    this.selectedBus = new Bus();
    this.toggleEditDialog();
  }

  public toggleEditDialog(): void {
    this.isEditDialogActive = !this.isEditDialogActive;
  }

  public saveChanges(): void {
    if (!!this.selectedBus.id) {
      this.busService.update(this.selectedBus)
        .pipe(
          tap(() => {
            this.loadBuses(this.lastLoadEvent);
            this.messageService.add({severity: 'success', summary: 'Готов', detail: 'Изменения сохранены'});
          }),
          catchError((err: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('bus_update_error', err.status, err.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось сохранить изменения'});
            return of(err);
          })
        ).subscribe();
    } else {
      this.busService.create(this.selectedBus)
        .pipe(
          tap(() => {
            this.loadBuses(this.lastLoadEvent);
            this.messageService.add({severity: 'success', summary: 'Готов', detail: 'Автобус добавлен'});
          }),
          catchError((err: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('bus_creation_error', err.status, err.message);
            this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось добавить автобус'});
            return of(err);
          })
        ).subscribe();
    }
    this.toggleEditDialog();
  }

  public edit(bus: Bus): void {
    this.selectedBus = bus;
    this.toggleEditDialog();
  }

  public loadBuses(event: LazyLoadEvent): void {
    this.loading = true;
    this.busService.loadAll(
      event.rows,
      event.first / event.rows,
      event.sortField || 'number',
      event.sortOrder
    ).subscribe((response: PageableResponse<Bus>) => {
      this.loading = false;
      this.lastLoadEvent = event;
      this.lastLoadEvent.first = 0;
      this.buses = response.content.filter(it => !it.isDeleted);
      this.totalRecords = response.totalElements;
    });
  }

  public delete(bus: Bus): void {
    this.confirmationService.confirm({
      message: 'Хотите удалить автобус?',
      accept: () => {
        this.busService.delete(bus)
            .pipe(
                tap(() => this.loadBuses(this.lastLoadEvent)),
                catchError((response) => {
                  this.analyticsService.errorEmitter('bus_deletion_error', response.status, response.message);
                  this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось удалить автобус'});
                  return of(response);
                })
            ).subscribe();
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Автобус удален'});
      },
      reject: () => {
      }
    });
  }

}
