import {Component, OnInit} from '@angular/core';
import {Route} from '../../entities/route';
import {RouteService} from '../../services/route.service';
import {Search} from '../../entities/search';
import {City} from '../../entities/city';
import {UserService} from '../../services/user.service';
import {User} from '../../entities/user';
import {Router} from '@angular/router';
import {PrimeNGConfig} from 'primeng/api';
import {Role} from '../../entities/role.enum';
import {RouteStatus} from '../../entities/route-status.enum';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';
import {catchError, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {of, Subscription} from 'rxjs';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  public syncRoutesEventSubscription: Subscription;

  public searchedRoutes: Route[] = [];
  public departurePoint: City;
  public minDate: Date = new Date();
  public maxDate: Date = new Date();
  public cities: City[];
  public isBookingForm = false;
  public selectedRoute: Route;
  public date: Date;
  public user: User;
  public weekdayToMaxDaysToSelect = new Map<number, number>([
    [1, 7], [2, 7], [3, 7], [4, 7], [5, 6], [6, 9], [0, 8]
  ]);
  public compareCity = SearchFormComponent._compareCity.bind(this);

  constructor(private routeService: RouteService,
              private userService: UserService,
              private router: Router,
              private config: PrimeNGConfig,
              private analyticsService: GoogleAnalyticsService,
              private sharedService: SharedService) {
    this.syncRoutesEventSubscription = this.sharedService.getSyncRoutesEvent().subscribe(() => {
      this.getRoutes();
    });
  }

  private static _compareCity(a, b): boolean {
    return !!a && !!b ? a.id === b.id : false;
  }

  public ngOnInit(): void {
    this.setCalendar();
    this.maxDate.setDate(this.minDate.getDate() + this.weekdayToMaxDaysToSelect.get(this.minDate.getDay()));
    this.routeService.getCities().subscribe(
      data => {
        this.cities = data;
        this.departurePoint = this.cities[0];
      }
    );
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
    });
  }

  public getRoutes(): void {
    if (!!this.departurePoint.id && !!this.date) {
      this.routeService.search(new Search(this.departurePoint.id, new Date(this.date)))
        .pipe(
          tap((data) => {
            this.searchedRoutes = data;
            this.analyticsService.eventEmitter('routes_search', 'route');
          }),
          catchError((response: HttpErrorResponse) => {
            this.analyticsService.errorEmitter('routes_search_error', response.status, response.message);
            return of(response);
          })
        ).subscribe();
      this.isBookingForm = false;
    }
  }

  public toggleBookingForm(route: Route): boolean {
    if (route.freePlaces > 0) {
      this.selectedRoute = route;
      this.isBookingForm = true;
    }
    return this.isBookingForm;
  }

  public isSelected(route: Route): boolean {
    return route === this.selectedRoute;
  }

  public openRoutePassengers(route: Route): void {
    this.selectedRoute = route;
    this.router.navigate(['dispatcher', {tab: 'passengers', routeId: this.selectedRoute.id}]);
  }

  public isAdministration(user: User): boolean {
    return user.roles.some(it => [Role.ADMIN, Role.DISPATCHER].includes(it));
  }

  public isActive(route: Route): boolean {
    return route.status === RouteStatus.ACTIVE
      && new Date(route.departureDateTime).getTime() > new Date().getTime();
  }

  public clearData(): void {
    this.searchedRoutes = [];
  }

  private setCalendar(): void {
    this.config.setTranslation({
      accept: 'Accept',
      reject: 'Cancel',
      dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
        'Ноябрь', 'Декабрь'],
      today: 'Сегодня',
      clear: 'Очистить'
    });
  }

}
