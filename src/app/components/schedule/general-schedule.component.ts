import {Component, OnInit} from '@angular/core';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

@Component({
  selector: 'app-general-schedule',
  templateUrl: './general-schedule.component.html',
  styleUrls: ['./general-schedule.component.css']
})
export class GeneralScheduleComponent implements OnInit {

  constructor(public analyticsService: GoogleAnalyticsService) {
  }

  ngOnInit(): void {
    this.analyticsService.eventEmitter('schedule_page', 'page_view');
  }

}
