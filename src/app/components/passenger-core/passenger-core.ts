import {PassengerService} from '../../services/passenger.service';
import {UserService} from '../../services/user.service';
import {RouteService} from '../../services/route.service';
import {Passenger} from '../../entities/passenger';
import {catchError, tap} from 'rxjs/operators';
import {ConfirmationService} from 'primeng/api';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';
import {DatePipe} from '@angular/common';

export abstract class PassengerCore {

  public passengers: Passenger[] = [];
  public selectedPassengerId: number;
  public isEditDialogActive = false;
  public onlyActive = false;
  public passengerRouteId: number;

  protected constructor(protected passengerService: PassengerService,
                        protected userService: UserService,
                        protected routeService: RouteService,
                        protected confirmationService: ConfirmationService,
                        protected analyticsService: GoogleAnalyticsService,
                        protected datePipe: DatePipe) {
  }

  public isActive(passenger: Passenger): boolean {
    return this.passengerService.isActive(passenger);
  }

  public removePassenger(passenger: Passenger): void {
    this.confirmationService.confirm({
      message: 'Хотите отменить заказ?',
      accept: () => {
        this.passengerService.deletePassenger(passenger.id)
          .pipe(
            tap((response) => {
              this.analyticsService.eventEmitter('passenger_deleted', 'passenger');
              return this.onRemovePassenger(response);
            }),
            catchError((error: HttpErrorResponse) => {
              this.analyticsService.errorEmitter('passenger_deletion_error', error.status, error.message);
              return of(error);
            })
          ).subscribe();
      },
      reject: () => {
      }
    });
  }

  public confirmPassenger(passenger: Passenger): void {
    this.confirmationService.confirm({
      message: 'Хотите подтвердить заказ?',
      accept: () => {
        passenger.confirmed = true;
        this.passengerService.updatePassengerConfirmation(passenger.id, passenger.confirmed)
            .pipe(
                tap((response) => {
                  this.analyticsService.eventEmitter('passenger_confirmed', 'passenger');
                  this.onConfirmPassenger(response);
                }),
                catchError((error: HttpErrorResponse) => {
                  this.analyticsService.errorEmitter('passenger_confirmation_error', error.status, error.message);
                  return of(error);
                })
            ).subscribe();
      },
      reject: () => {
      }
    });
  }

  public toggleEditDialog(): void {
    this.isEditDialogActive = !this.isEditDialogActive;
  }

  public cancelChanges(): void {
    this.toggleEditDialog();
    this.selectedPassengerId = null;
  }

  public abstract onRemovePassenger(response: any): any;

  public abstract onConfirmPassenger(response: any): any;

}
