import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {User} from '../../entities/user';
import {Point} from '../../entities/point';
import {UserService} from '../../services/user.service';
import {RouteService} from '../../services/route.service';
import {Passenger} from '../../entities/passenger';
import {PassengerService} from '../../services/passenger.service';
import {Route} from '../../entities/route';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {RouteStatus} from '../../entities/route-status.enum';
import {MessageService} from 'primeng/api';
import {UserRating} from '../../entities/user-rating.enum';
import {UserEmpty} from '../../entities/user-empty';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {ThemeService} from '../../services/theme.service';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-edit-passenger-form',
  templateUrl: './edit-passenger-form.component.html',
  styleUrls: ['./edit-passenger-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditPassengerFormComponent implements OnInit, OnDestroy {

  @Output() public changesSubmit = new EventEmitter<Passenger>();
  @Output() public cancelChanges = new EventEmitter<void>();
  public user: User;
  public route: Route;
  public passenger: Passenger;
  public departurePoints: Point[];
  public arrivalPoints: Point[];
  public defaultArrivalTime: string;
  public passengerExists = false;
  public maxNumberOfPlaces: number;
  public newNumberOfPlaces = 1;
  public isRatingOpen = false;
  public existedPassengerId: number;
  public comparePoint = EditPassengerFormComponent._comparePoint.bind(this);
  public isDarkTheme = false;
  public invalidName = false;
  public invalidPhone = false;
  public invalidDeparturePoint = false;
  public invalidArrivalPoint = false;

  private readonly PHONE_LENGTH = 9;

  constructor(public userService: UserService,
              public routeService: RouteService,
              public passengerService: PassengerService,
              private changeDetector: ChangeDetectorRef,
              private messageService: MessageService,
              private analyticsService: GoogleAnalyticsService,
              private router: Router,
              private themeService: ThemeService,
              private sharedService: SharedService) {
  }

  @Input() set passengerId(passengerId: number) {
    this.passenger = new Passenger();
    this.existedPassengerId = null;
    if (!!passengerId) {
      (this.userService.isCurrentAdministration()
          ? this.passengerService.getPassengerByIdAsAdmin(passengerId)
          : this.passengerService.getPassengerById(passengerId)
      ).subscribe(data => {
        this.passenger = data;
        this.newNumberOfPlaces = this.passenger.numberOfPlaces;
        this.changeDetector.detectChanges();
      });
    } else {
      this.setUserCredentials();
    }
    this.changeDetector.detectChanges();
  }

  @Input() set routeId(routeId: number) {
    if (!!routeId) {
      (this.isAdministration()
          ? this.routeService.getRouteByIdAsAdmin(routeId)
          : this.routeService.getRouteById(routeId)
      ).subscribe(data => {
        this.route = data;
        this.maxNumberOfPlaces = this.getMaxNumberOfPlaces();
        this.changeDetector.markForCheck();
      });
      this.routeService.getPointsOfRoute(routeId).subscribe(data => {
        if (!this.isAdministration()) {
          this.departurePoints = data.landingPoints.filter(it => !it.adminOnly);
          this.arrivalPoints = data.dropOffPoints.filter(it => !it.adminOnly);
        } else {
          this.departurePoints = data.landingPoints;
          this.arrivalPoints = data.dropOffPoints;
        }
        this.defaultArrivalTime = this.arrivalPoints.filter(el => el.name === 'Автостанция г.Лунинец' || el.name === 'АВ Центральный')
          .pop().time;
        this.changeDetector.markForCheck();
      });
      this.changeDetector.detectChanges();
    }
  }

  private static _comparePoint(a, b): boolean {
    return !!a && !!b ? a.id === b.id : false;
  }

  public canModify(passenger: Passenger): boolean {
    return passenger && !passenger.id
      || (!!passenger.route && Math.abs((new Date(passenger.route.departureDateTime).getTime() - Date.now()) / 36e5) > 1);
  }

  public ngOnDestroy(): void {
    this.passenger = null;
    this.passengerExists = false;
    this.clearValidation();
  }

  public ngOnInit(): void {
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
      this.setUserCredentials();
      this.changeDetector.detectChanges();
    });
  }

  public saveChanges(): void {
    this.clearValidation();
    if (this.isValidPassenger()) {
      if (!this.passenger.id) {
        this.createPassenger();
      } else {
        this.updatePassenger();
      }
    } else {
      this.validateName();
      this.validatePhone();
      this.validateArrivalPoint();
      this.validateDeparturePoint();
    }
  }

  private clearValidation(): void {
    this.invalidPhone = false;
    this.invalidName = false;
    this.invalidDeparturePoint = false;
    this.invalidArrivalPoint = false;
  }

  private validateDeparturePoint(): void {
    if (!this.passenger.departurePoint) {
      this.invalidDeparturePoint = true;
    }
  }

  private validateArrivalPoint(): void {
    if (!this.passenger.arrivalPoint) {
      this.invalidArrivalPoint = true;
    }
  }

  private validatePhone(): void {
    if (!this.passenger.user.phone || this.passenger.user.phone.length !== 9) {
      this.invalidPhone = true;
    }
  }

  private validateName(): void {
    if (!this.passenger.name) {
      this.invalidName = true;
    }
  }

  public cancel(): void {
    this.cancelChanges.emit();
    this.ngOnDestroy();
  }

  public isValidPassenger(): boolean {
    return !!this.passenger.name
      && !!this.passenger.user.phone && this.passenger.user.phone.length === this.PHONE_LENGTH
      && !!this.passenger.arrivalPoint
      && !!this.passenger.departurePoint;
  }

  public isActive(passenger: Passenger): boolean {
    return !!passenger && !!passenger.id
      ? this.passengerService.isActive(passenger) && passenger.route.status === RouteStatus.ACTIVE
      : true;
  }

  public isAdministration(): boolean {
    return this.userService.isCurrentAdministration();
  }

  public isAdmin(): boolean {
    return this.userService.isCurrentAdmin();
  }

  public toggleConfirmed(passenger: Passenger): void {
    passenger.confirmed = !passenger.confirmed;
    if (passenger.id) {
      this.passengerService.updatePassengerConfirmation(passenger.id, passenger.confirmed).subscribe(data =>
        this.passenger = data);
    }
  }

  public isPlusButtonDisabled(): boolean {
    return this.isAdministration()
      ? (!!this.passenger.numberOfPlaces
        ? this.newNumberOfPlaces >= this.passenger.numberOfPlaces
        && this.newNumberOfPlaces - this.passenger.numberOfPlaces >= this.maxNumberOfPlaces
        : this.newNumberOfPlaces >= this.maxNumberOfPlaces)
      : (!!this.passenger.numberOfPlaces
        ? this.newNumberOfPlaces >= this.passenger.numberOfPlaces
        && (this.newNumberOfPlaces < 4 ? this.newNumberOfPlaces - this.passenger.numberOfPlaces >= this.maxNumberOfPlaces : true)
        : this.newNumberOfPlaces >= this.maxNumberOfPlaces);
  }

  public getExistedUser(): void {
    this.userService.getByPhone(this.passenger.user.phone).subscribe(data => {
      this.passenger.name = data.name;
      this.passenger.user.rating = data.rating;
      this.changeDetector.detectChanges();
    }, () => {
      this.passenger.name = null;
      this.changeDetector.detectChanges();
    });
  }

  public toggleRatingBox(): void {
    this.isRatingOpen = !this.isRatingOpen;
  }

  public updateRating(value: string): void {
    const rating = UserRating[value];
    this.passenger.user.rating = rating;
    this.userService.updateRating(this.passenger.user.id, rating)
      .pipe(
        tap(() => {
          this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Рейтинг изменен'});
        }),
        catchError((response) => {
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось изменить рейтинг'});
          return of(response);
        })
      ).subscribe();
  }

  public processName(value: string): void {
    this.passenger.name = value.split(' ').map(word => word[0].toUpperCase() + word.substring(1)).join(' ');
  }

  public reorder(): void {
    this.passengerService.reorder(this.existedPassengerId, this.passenger)
      .pipe(
        tap(() => {
          this.analyticsService.eventEmitter('passenger_reordered', 'passenger');
          this.changesSubmit.emit();
          this.ngOnDestroy();
          this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Бронь перенесена'});
        }),
        catchError((response: HttpErrorResponse) => {
          this.analyticsService.errorEmitter('passenger_reorder_error', response.status, response.message);
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось перенести бронь'});
          return of(response);
        })
      ).subscribe();
  }

  public updateUserName(value: string): void {
    this.userService.updateName(this.passenger.user.phone, value).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Имя пользователя изменено'});
    });
  }

  private createPassenger(): void {
    this.passenger.route = this.route;
    this.passenger.numberOfPlaces = this.newNumberOfPlaces;
    (this.isAdministration()
        ? this.passengerService.createPassengerAsAdmin(this.passenger)
        : this.passengerService.createPassenger(this.passenger)
    ).pipe(
      tap((data) => {
        this.analyticsService.eventEmitter('passenger_created', 'passenger');
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Место забронировано'});
        this.changesSubmit.emit(data);
      }),
      catchError((error: any) => {
        this.analyticsService.errorEmitter('passenger_creation_error', error.status, error.message);
        this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось забронировать место'});
        if (error.error.code === '409.01') {
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не достаточно свободных мест'});
          this.sharedService.sendSyncRoutesEvent();
        }
        if (error.error.code === '409.02' && !!error.error.errors[this.passenger.user.phone]) {
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Вы уже записаны на рейс выбраннной даты и направления'});
          this.passengerExists = true;
          this.existedPassengerId = error.error.errors[this.passenger.user.phone];
        }
        return of(error);
      })
    ).subscribe(() => {
      this.changeDetector.detectChanges();
    });
  }

  private getMaxNumberOfPlaces(): number {
    return this.userService.isCurrentAdministration()
      ? this.route.freePlaces
      : Math.min(4, this.route.freePlaces);
  }

  private updatePassenger(): void {
    this.passenger.numberOfPlaces = this.newNumberOfPlaces;
    (this.isAdministration()
        ? this.passengerService.updatePassengerAsAdmin(this.passenger)
        : this.passengerService.updatePassenger(this.passenger)
    ).pipe(
      tap(() => {
        this.changesSubmit.emit();
        this.ngOnDestroy();
        this.analyticsService.eventEmitter('passenger_updated', 'passenger');
        this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Изменения сохранены'});
      }),
      catchError((error: HttpErrorResponse) => {
        this.analyticsService.errorEmitter('passenger_updating_error', error.status, error.message);
        this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось внести изменения'});
        return of(error);
      })
    ).subscribe();
  }

  private setUserCredentials(): void {
    if (!!this.passenger) {
      if (!!this.user && !this.isAdministration()) {
        this.passenger.user = this.user;
        this.passenger.name = this.user.name;
      } else {
        this.passenger.user = new UserEmpty();
      }
    }
  }

  private setTheme(): void {
    if (this.themeService.getActiveTheme().name === 'dark') {
      this.router.events.subscribe(event => {
        this.isDarkTheme = this.router.url === '/' || this.router.url === '/dispatcher;tab=search';
      });
    }
  }

}
