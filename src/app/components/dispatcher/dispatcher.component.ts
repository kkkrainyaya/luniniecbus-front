import {Component, OnInit} from '@angular/core';
import {User} from '../../entities/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-dispatcher',
  templateUrl: './dispatcher.component.html',
  styleUrls: ['./dispatcher.component.css']
})
export class DispatcherComponent implements OnInit {

  public isSearch = true;
  public isPassengers = false;
  public routeId: number;
  public user: User;

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
    });
    this.route.params.subscribe(params => {
      if (params) {
        if (params.tab === 'passengers') {
          this.routeId = params.routeId;
          this.togglePassengers();
        }
        if (params.tab === 'search') {
          this.toggleSearch();
        }
      }
    });
  }

  toggleSearch(): void {
    if (!this.isSearch) {
      this.isSearch = !this.isSearch;
      this.isPassengers = false;
      this.routeId = null;
    }
    this.router.navigate(['dispatcher', {tab: 'search'}]);
  }

  togglePassengers(): void {
    if (!this.isPassengers) {
      this.isSearch = false;
      this.isPassengers = !this.isPassengers;
    }
    if (!!this.routeId) {
      this.router.navigate(['dispatcher', {tab: 'passengers', routeId: this.routeId}]);
    } else {
      this.router.navigate(['dispatcher', {tab: 'passengers'}]);
    }
  }

}
