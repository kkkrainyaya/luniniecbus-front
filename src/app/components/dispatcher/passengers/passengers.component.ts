import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {Passenger} from '../../../entities/passenger';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {PassengerService} from '../../../services/passenger.service';
import {PageableResponse} from '../../../entities/pageable-response';
import {RouteService} from '../../../services/route.service';
import {User} from '../../../entities/user';
import {UserService} from '../../../services/user.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {Route} from '../../../entities/route';
import {PassengerCore} from '../../passenger-core/passenger-core';
import {RouteStatus} from '../../../entities/route-status.enum';
import {BusService} from '../../../services/bus.service';
import {Bus} from '../../../entities/bus';
import {Point} from '../../../entities/point';
import {City} from '../../../entities/city';
import {catchError, tap} from 'rxjs/operators';
import {GoogleAnalyticsService} from '../../../services/google-analytics.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-passengers',
  templateUrl: './passengers.component.html',
  styleUrls: ['./passengers.component.css']
})
export class PassengersComponent extends PassengerCore implements OnInit {

  @ViewChild(Table) public passengersTable: Table;
  public currentRouteId: number;
  public loading = true;
  public totalRecords: number;
  public cities: string[] = [];
  public points: string[] = [];
  public user: User;
  public lastLoadEvent: LazyLoadEvent;
  public filters = new Map<string, string>();
  public totalPassengers = 0;
  public route: Route = null;
  public selectedRoute: Route;
  public buses: Bus[] = [];
  public drivers: User[] = [];
  public currentBus: string;
  public pointsMap = new Map<number, Point>();
  public citiesMap = new Map<number, City>();
  public compareBuses = PassengersComponent._compareById.bind(this);
  public compareDrivers = PassengersComponent._compareById.bind(this);

  constructor(protected passengerService: PassengerService,
              protected routeService: RouteService,
              protected userService: UserService,
              protected router: Router,
              protected confirmationService: ConfirmationService,
              protected analyticsService: GoogleAnalyticsService,
              protected datePipe: DatePipe,
              private messageService: MessageService,
              private busService: BusService) {
    super(passengerService, userService, routeService, confirmationService, analyticsService, datePipe);
  }

  private static _compareById(a, b): boolean {
    return !!a && !!b ? a.id === b.id : false;
  }

  public ngOnInit(): void {
    this.loadRoute();
    this.getPoints();
    this.getCities();
    this.getCurrentUserData();
    if (this.currentRouteId) {
      this.getBuses();
      this.getDrivers();
    }
  }

  @Input() set routeId(routeId: number) {
    this.currentRouteId = routeId;
  }

  public loadPassengers(event: LazyLoadEvent): void {
    this.loading = true;
    this.passengerService.loadPassengers(
      event.rows,
      event.first / event.rows,
      event.sortField || (!!this.currentRouteId ? 'creationDate' : 'route.date'),
      event.sortOrder,
      this.filters,
      this.currentRouteId,
      null,
      this.onlyActive,
    ).subscribe((resp: PageableResponse<Passenger>) => {
      this.lastLoadEvent = event;
      this.lastLoadEvent.first = 0;
      this.passengers = resp.content;
      this.loading = false;
      this.totalRecords = resp.totalElements;
      if (!!this.passengers) {
        this.totalPassengers = this.passengers.reduce((sum, current) => sum + current.numberOfPlaces, 0);
      }
    });
  }

  public onRemovePassenger(response: any): any {
    this.loadPassengers(this.lastLoadEvent);
    this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Пассажир удален'});
    return of(response);
  }

  public onConfirmPassenger(response: any): any {
    this.loadPassengers(this.lastLoadEvent);
    this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Заказ подтвержден'});
    return of(response);
  }

  public addNewPassenger(): void {
    this.selectedPassengerId = undefined;
    if (!!this.currentRouteId) {
      this.selectedRoute = this.route;
      this.toggleEditDialog();
    } else {
      this.router.navigate(['dispatcher', {tab: 'search'}]).then(page => {
        window.location.reload();
      });
    }
  }

  public refreshPassengers(): void {
    this.toggleEditDialog();
    this.selectedPassengerId = null;
    this.loadRoute();
    this.loadPassengers(this.lastLoadEvent);
  }

  public filter(field: string, value: string): void {
    if (!!value) {
      if (field === 'date') {
        value = new Date(value).toLocaleDateString();
      }
      this.filters.set(field, value);
    } else {
      this.filters.delete(field);
    }
    this.loadPassengers(this.lastLoadEvent);
  }

  public clear(): void {
    this.filters.clear();
    this.loadPassengers(this.lastLoadEvent);
  }

  public toggleActive(): void {
    this.onlyActive = !this.onlyActive;
    this.loadPassengers(this.lastLoadEvent);
  }

  public showAllPassengers(): void {
    this.router.navigate(['dispatcher', {tab: 'passengers'}]).then(page => {
      window.location.reload();
    });
  }

  public showPassenger(passenger: Passenger): void {
    this.selectedPassengerId = passenger.id;
    if (!!this.route) {
      this.selectedRoute = this.route;
    }
    this.routeService.getRouteByIdAsAdmin(passenger.route.id).subscribe(data => this.selectedRoute = data);
    this.passengerRouteId = passenger.route.id;
    this.toggleEditDialog();
  }

  public toggleEditDialog(): void {
    this.isEditDialogActive = !this.isEditDialogActive;
    if (!this.isEditDialogActive) {
      this.selectedRoute = null;

    }
  }

  public updateRouteNotes(route: Route, value: string): void {
    route.notes = value;
    this.update(route);
  }

  public updateDriver(route: Route): void {
    if (!!route.driver) {
      this.update(route);
    }
  }

  public updateBus(route: Route): void {
    if (!!route.bus) {
      this.update(route);
    }
  }

  public updateWaybillNumber(route: Route, value: string): void {
    route.waybillNumber = value;
    this.update(route);
  }

  public isRouteActive(): boolean {
    return this.route.status === RouteStatus.ACTIVE
      && new Date(this.route.departureDateTime).getTime() > new Date().getTime();
  }

  public freePlacesLeft(): boolean {
    return this.route.freePlaces > 0;
  }

  private update(route: Route): void {
    this.routeService.update(route)
      .pipe(tap(() => {
          this.messageService.add({severity: 'success', summary: 'Готово', detail: 'Изменения сохранены'});
        }),
        catchError((response) => {
          this.analyticsService.errorEmitter('route_updating_error', response.status, response.message);
          this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Не удалось внести изменения'});
          return of(response);
        })).subscribe();
  }

  private getCurrentUserData(): void {
    this.userService.currentUser$.subscribe(user => {
      this.user = user;
    });
  }

  private getCities(): void {
    this.routeService.getCities().subscribe(data => {
      data.forEach(it => {
        this.citiesMap.set(it.id, {...it});
        this.cities.push(it.name);
      });
    });
  }

  private getPoints(): void {
    this.routeService.getAllPoints().subscribe(data => {
      data.forEach(it => {
        this.pointsMap.set(it.id, {...it});
        this.points.push(it.name);
      });
    });
  }

  private loadRoute(): void {
    if (!!this.currentRouteId) {
      this.routeService.getRouteByIdAsAdmin(this.currentRouteId).subscribe(date => {
        this.route = date;
      });
    }
  }

  private getBuses(): void {
    this.busService.getBuses().subscribe(data => {
      this.buses = data;
    });
  }

  private getDrivers(): void {
    this.userService.getDrivers().subscribe(data => {
      this.drivers = data;
    });
  }
}
