import {RouteData} from '../entities/route-data';

class RootRoutes extends RouteData {
  readonly LOGIN = 'login';
  readonly ABOUT = 'about';
  readonly PRIVACY = 'privacy';
  readonly SCHEDULE = 'schedule';
  readonly PERSONAL = 'personal';
  readonly ADMIN = 'admin';
  readonly DISPATCHER = 'dispatcher';
  readonly STATISTICS = 'statistics';
  readonly HELP = 'help';
}

class PersonalRoutes extends RouteData {
  readonly ORDERS = 'personal/orders';
  readonly PROFILE = 'personal/profile';
}

class AdminRoutes extends RouteData {
  readonly ROUTES = 'admin/routes';
  readonly PASSENGERS = 'admin/passengers';
}

class HelpRoutes extends RouteData {
  readonly BOOKING = 'help/booking';
}

export class RouteConstants {
  static readonly ROOT = new RootRoutes(null, '/');
  static readonly ABOUT = new RootRoutes(RouteConstants.ROOT, RouteConstants.ROOT.ABOUT);
  static readonly PRIVACY = new RootRoutes(RouteConstants.ROOT, RouteConstants.ROOT.PRIVACY);
  static readonly SCHEDULE = new RootRoutes(RouteConstants.ROOT, RouteConstants.ROOT.SCHEDULE);
  static readonly LOGIN = new RootRoutes(RouteConstants.ROOT, RouteConstants.ROOT.LOGIN);
  static readonly PERSONAL = new PersonalRoutes(RouteConstants.ROOT, RouteConstants.ROOT.PERSONAL);
  static readonly ADMIN = new AdminRoutes(RouteConstants.ROOT, RouteConstants.ROOT.ADMIN);
  static readonly DISPATCHER = new AdminRoutes(RouteConstants.ROOT, RouteConstants.ROOT.DISPATCHER);
  static readonly PASSENGERS = new AdminRoutes(RouteConstants.ADMIN, RouteConstants.ADMIN.PASSENGERS);
  static readonly ROUTES = new AdminRoutes(RouteConstants.ADMIN, RouteConstants.ADMIN.ROUTES);
  static readonly STATISTICS = new RootRoutes(RouteConstants.ROOT, RouteConstants.ROOT.STATISTICS);
  static readonly HELP = new HelpRoutes(RouteConstants.ROOT, RouteConstants.ROOT.HELP);
  static readonly HELP_BOOKING = new HelpRoutes(RouteConstants.HELP, RouteConstants.HELP.BOOKING);
}
