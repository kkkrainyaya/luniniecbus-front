import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {ThemeService} from './services/theme.service';

declare const gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'front';
  isBackgroundVisible = false;
  isBlackBackgroundVisible = false;

  constructor(private router: Router,
              private themeService: ThemeService) {
    this.setUpAnalytics();
  }

  setUpAnalytics(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        gtag('config', 'G-WK0QWHGJ9N', {
          page_path: event.urlAfterRedirects
        });
      });
  }

  ngOnInit(): void {
    this.setDarkTheme();
  }

  private setDarkTheme(): void {
    this.themeService.setDarkTheme();
    this.router.events.subscribe(event => {
      this.isBlackBackgroundVisible = this.router.url === '/' || this.router.url === '/dispatcher;tab=search';
    });
  }

  private setDefaultTheme(): void {
    this.themeService.setDefaultTheme();
    this.router.events.subscribe(event => {
      this.isBackgroundVisible = this.router.url === '/' || this.router.url === '/dispatcher;tab=search';
    });
  }
}
