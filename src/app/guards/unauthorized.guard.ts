import {Injectable} from '@angular/core';
import {CanLoad, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {UserService} from '../services/user.service';
import {UserEmpty} from '../entities/user-empty';

@Injectable()
export class UnauthorizedGuard implements CanLoad {

  constructor(private router: Router,
              private currentUserService: UserService,
  ) {
  }

  public canLoad(): Observable<boolean> {
    return this.currentUserService.currentUser$.pipe(
      map(user => user instanceof UserEmpty),
      tap(canActivate => {
        if (!canActivate) {
          this.router.navigate(['/']);
        }
      }));
  }
}
