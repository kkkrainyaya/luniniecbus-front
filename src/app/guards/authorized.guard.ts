import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {RouteConstants} from '../constants/route-constants';
import {UserEmpty} from '../entities/user-empty';
import {UserService} from '../services/user.service';
import {HttpStatus} from '../constants/http-status.enum';

@Injectable()
export class AuthorizedGuard implements CanActivate {

  constructor(private router: Router,
              private currentUserService: UserService,
  ) {
  }

  public canActivate(): Observable<boolean> {
    return this.currentUserService.currentUser$.pipe(
      map(user => !(user instanceof UserEmpty)),
      tap(canActivate => {
        if (!canActivate) {
          this.router.navigate(RouteConstants.ROOT.getFullUrl(RouteConstants.ROOT.LOGIN));
        }
      }), catchError((err) => {
        if (!!err.status && err.status === HttpStatus.Unauthorized) {
          this.router.navigate(RouteConstants.ROOT.getFullUrl(RouteConstants.ROOT.LOGIN));
        }
        return of(err);
      }));
  }
}
