import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {RouteConstants} from '../constants/route-constants';
import {UserEmpty} from '../entities/user-empty';
import {UserService} from '../services/user.service';
import {Role} from '../entities/role.enum';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router,
              private currentUserService: UserService,
  ) {
  }

  public canActivate(): Observable<boolean> {
    return this.currentUserService.currentUser$.pipe(
      map(user => !(user instanceof UserEmpty)
          && user.roles.some(it => [Role.ADMIN, Role.DISPATCHER].includes(it))),
        tap(canActivate => {
          if (!canActivate) {
            this.router.navigate(RouteConstants.ROOT.getFullUrl(RouteConstants.ROOT.LOGIN));
          }
        }));
  }
}
