import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {TokenStorageService} from '../services/token-storage.service';
import {catchError, map, switchMap, take} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {HttpStatus} from '../constants/http-status.enum';
import {RouteConstants} from '../constants/route-constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenStorageService,
              private router: Router) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.applyToken(req)
      .pipe(switchMap((request: HttpRequest<any>) =>
        next.handle(request).pipe(
          catchError(err => {
            this.redirectOnUnauthorized(err);
            return throwError(err);
          }))
      ));
  }

  private applyToken(req: HttpRequest<any>): Observable<HttpRequest<any>> {
    return this.tokenService.currentToken$.pipe(
      take(1),
      map((token: string) =>
        !token ? req : req.clone({headers: req.headers.set('x-auth-token', token)})));
  }

  private redirectOnUnauthorized(err: any): void {
    if (err.status === HttpStatus.Unauthorized) {
      this.tokenService.clear();
      this.router.navigate(RouteConstants.ROOT.getFullUrl(RouteConstants.ROOT.LOGIN));
    }
  }

}
