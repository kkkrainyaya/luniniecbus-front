import {HttpParams} from '@angular/common/http';

export class SortingUtility {

  public static mapParams(size: number,
                          page: number,
                          sortFields: Map<string, number>,
                          filters: Map<string, string>): HttpParams {
    let params = new HttpParams().set('size', `${size}`).set('page', `${page}`);
    if (!!sortFields && sortFields.size > 0) {
      sortFields.forEach((order, field) => {
        const sortParam = SortingUtility.getSortParam(field, order);
        params = params.append('sort', `${sortParam}`);
      });
    }
    if (!!filters && filters.size > 0) {
      filters.forEach((value, field) => {
        params = params.set(field, `${value}`);
      });
    }
    return params;
  }

  private static getSortParam(field: string, order: number): string {
    return `${field},${order > 0 ? 'ASC' : 'DESC'}`;
  }

}
