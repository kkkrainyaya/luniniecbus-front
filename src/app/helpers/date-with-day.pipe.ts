import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  name: 'dateWithDay'
})
export class DateWithDayPipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {
  }

  private daysOfWeek = {
    0: 'Вс',
    1: 'Пн',
    2: 'Вт',
    3: 'Ср',
    4: 'Чт',
    5: 'Пт',
    6: 'Сб',
    7: 'Вс'
  };

  transform(value: Date): string {
    value = new Date(value);
    const transformedDate = this.datePipe.transform(value, 'dd.MM.yyyy HH:mm');
    return `${this.daysOfWeek[value.getDay()]} ${transformedDate}`;
  }

}
