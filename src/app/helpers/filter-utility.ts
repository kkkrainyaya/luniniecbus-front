import {FilterOperations} from '../entities/filter-operations.enum';

export class FilterUtility {

  public static getOperation(operator: string): string {
    return Object.entries(FilterOperations)
      .find((operation) => operation[0] === operator)[1];
  }

  public static buildFilterEntry(key: string, operator: string, value: string): string {
    return key + FilterUtility.getOperation(operator) + value;
  }

}
