import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private syncRoutes = new Subject<any>();

  sendSyncRoutesEvent(): void {
    this.syncRoutes.next();
  }
  getSyncRoutesEvent(): Observable<any>{
    return this.syncRoutes.asObservable();
  }

}
