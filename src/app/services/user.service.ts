import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, of, ReplaySubject} from 'rxjs';
import {User} from '../entities/user';
import {Login} from '../entities/login';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserIdleService} from 'angular-user-idle';
import {TokenStorageService} from './token-storage.service';
import {UserEmpty} from '../entities/user-empty';
import {RouteConstants} from '../constants/route-constants';
import {PageableResponse} from '../entities/pageable-response';
import {SortingUtility} from '../helpers/sorting-utility';
import {Role} from '../entities/role.enum';
import {UserRating} from '../entities/user-rating.enum';

const API_URL = environment.baseUrl + '/api/v1/users';
const API_ADMIN_URL = environment.baseUrl + '/api/v1/admin/users';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const TOKEN_HEADER_KEY = 'x-auth-token';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserStream: ReplaySubject<User> = new ReplaySubject<User>(1);

  constructor(private http: HttpClient,
              private router: Router,
              private userIdle: UserIdleService,
              private tokenService: TokenStorageService) {
    this.userIdle.onTimeout().subscribe(() => {
      this.clear();
      this.router.navigate(RouteConstants.ROOT.getFullUrl(RouteConstants.ROOT.LOGIN));
    });
    this.userIdle.onTimerStart().subscribe();
    this.tokenService.currentToken$.pipe(
      switchMap((token: string) =>
        token === '' ? of(new UserEmpty()).pipe(take(1)) : this.getCurrentUserData()),
      tap((user) => this.next(user)))
      .subscribe();
  }

  private static setIsStaff(isAdministration: boolean, params: HttpParams): HttpParams {
    if (isAdministration) {
      params = params.set('isStaff', String(isAdministration));
    }
    return params;
  }

  public getCurrentUserData(): Observable<User> {
    return this.http.get<User>(API_URL + '/current', httpOptions);
  }

  public next(user: User): void {
    this.userIdle.resetTimer();
    this.userIdle.startWatching();
    this.currentUserStream.next(user);
  }

  public get currentUser$(): Observable<User> {
    return this.currentUserStream.asObservable();
  }

  public sendOtp(phone: string): Observable<any> {
    return this.http.post(API_URL + `/phone/${phone}/otp`, {}, {
      observe: 'response',
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public loginWithOtp(login: Login): Observable<User> {
    return this.http.post(API_URL + '/login/otp', JSON.stringify(login), {
      observe: 'response',
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }).pipe(map((resp: HttpResponse<any>) => {
      const user = resp.body;
      this.tokenService.next(resp.headers.get(TOKEN_HEADER_KEY));
      this.next(user);
      return user;
    }));
  }

  public loginWithPassword(login: Login): Observable<User> {
    return this.http.post(API_URL + '/login/password', JSON.stringify(login), {
      observe: 'response',
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }).pipe(map((resp: HttpResponse<any>) => {
      const user = resp.body;
      this.tokenService.next(resp.headers.get(TOKEN_HEADER_KEY));
      this.next(user);
      return user;
    }));
  }

  public update(user: User): Observable<User> {
    return this.http.put<User>(API_URL + `/${user.id}`, JSON.stringify(user), httpOptions);
  }

  public updateCurrentUser(user: User): Observable<User> {
    return this.http.put(API_URL + `/${user.id}`, JSON.stringify(user), httpOptions)
      .pipe(map((updatedUser: User) => {
        this.next(updatedUser);
        return updatedUser;
      }));
  }

  public updatePassword(user: User, newPassword: string): Observable<void> {
    return this.http.put<void>(API_URL + `/${user.id}/password`, newPassword, httpOptions);
  }

  public updateName(phone: string, newName: string): Observable<void> {
    return this.http.put<void>(API_URL + `/phone/${phone}/name`, newName, httpOptions);
  }

  public clear(): void {
    this.currentUserStream.next(new UserEmpty());
    this.tokenService.clear();
  }

  public loadAll(
    size: number,
    page: number,
    sortField?: string,
    sortOrder?: number,
    isStaff?: boolean,
  ): Observable<PageableResponse<User>> {
    let params: HttpParams = SortingUtility.mapParams(size, page, new Map([[sortField, sortOrder]]), null);
    params = UserService.setIsStaff(isStaff, params);
    return this.http.get<PageableResponse<User>>(API_ADMIN_URL, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public addEmployee(user: User): Observable<User> {
    return this.http.post<User>(`${API_ADMIN_URL}/staff`, JSON.stringify(user), httpOptions);
  }

  public getDrivers(): Observable<User[]> {
    return this.http.get<User[]>(`${API_ADMIN_URL}/drivers`, httpOptions);
  }

  public updateRole(user: User, roles: Role[]): Observable<void> {
    return this.http.put<void>(API_ADMIN_URL + `/${user.id}/role`, JSON.stringify(roles), httpOptions);
  }

  public getByPhone(phone: string): Observable<User> {
    return this.http.get<User>(`${API_ADMIN_URL}/phone/${phone}`, httpOptions);
  }

  public updateRating(id: number, rating: UserRating): Observable<void> {
    return this.http.put<void>(`${API_ADMIN_URL}/${id}/rating`, JSON.stringify(rating), httpOptions);
  }

  public isCurrentAdministration(): boolean {
    let user;
    this.currentUser$.subscribe(it => user = it);
    return !!user && user.roles.some(it => [Role.ADMIN, Role.DISPATCHER].includes(it));
  }

  public isCurrentAdmin(): boolean {
    let user;
    this.currentUser$.subscribe(it => user = it);
    return !!user && user.roles.some(it => [Role.ADMIN].includes(it));
  }

  public updateOnRepairStatus(driverId: number, isOnRepair: boolean, date: Date): Observable<void> {
    const params: HttpParams = new HttpParams()
        .set('onRepairDate', date.toLocaleDateString());
    return this.http.put<void>(API_ADMIN_URL + `/${driverId}/repair/${isOnRepair}`, null, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }
}
