import {Injectable} from '@angular/core';
import {dark, defaultTheme, Theme} from '../entities/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private active: Theme;
  private availableThemes: Theme[] = [dark, defaultTheme];

  constructor() {
  }

  setDefaultTheme(): void {
    this.setActiveTheme(defaultTheme);
  }

  setDarkTheme(): void {
    this.setActiveTheme(dark);
  }

  setActiveTheme(theme: Theme): void {
    this.active = theme;

    Object.keys(this.active.properties).forEach(property => {
      document.documentElement.style.setProperty(
        property,
        this.active.properties[property]
      );
    });
  }

  getActiveTheme(): Theme {
    return this.active;
  }
}
