import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';

@Injectable()
export class TokenStorageService {

  private static TOKEN_KEY = 'TOKEN';
  private currentTokenStream: ReplaySubject<string> = new ReplaySubject<string>(1);

  constructor() {
    const token: string = sessionStorage.getItem(TokenStorageService.TOKEN_KEY);
    if (token) {
      this.next(token);
    } else {
      this.next('');
    }
  }

  public get currentToken$(): Observable<string> {
    return this.currentTokenStream.asObservable();
  }

  public next(token: string): void {
    this.currentTokenStream.next(token);
    sessionStorage.setItem(TokenStorageService.TOKEN_KEY, token);
  }

  public clear(): void {
    this.currentTokenStream.next('');
    sessionStorage.clear();
  }

}
