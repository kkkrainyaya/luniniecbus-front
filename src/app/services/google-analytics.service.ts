import {Injectable} from '@angular/core';

declare let gtag: Function;

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor() {
  }

  public eventEmitter(
    eventName: string,
    eventCategory: string): void {
    gtag('event', eventName, {
      eventCategory
    });
  }

  public errorEmitter(
    errorName: string,
    errorCode: number,
    errorMessage: string): void {
    gtag('event', 'exception', {
      errorName,
      errorCode,
      errorMessage
    });
  }
}
