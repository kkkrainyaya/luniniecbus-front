import {Injectable} from '@angular/core';
import {Search} from '../entities/search';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {RoutePoints} from '../entities/route-points';
import {City} from '../entities/city';
import {Route} from '../entities/route';
import {Point} from '../entities/point';
import {PageableResponse} from '../entities/pageable-response';
import {SortingUtility} from '../helpers/sorting-utility';
import * as moment from 'moment';

const API_URL = environment.baseUrl + '/api/v1/routes';
const API_ADMIN_URL = environment.baseUrl + '/api/v1/admin/routes';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  constructor(private http: HttpClient) {
  }

  private static setOnlyActive(onlyActive: boolean, params: HttpParams): HttpParams {
    if (onlyActive) {
      const date = new Date();
      params = params.set('startTime', date.toLocaleTimeString())
        .set('startDate', date.toLocaleDateString());
    }
    return params;
  }

  search(search: Search): Observable<Route[]> {
    Date.prototype.toJSON = function (): string {
      return moment(this).format().split('+')[0];
    };
    return this.http.post<Route[]>(API_URL + `/search`, JSON.stringify(search), httpOptions);
  }

  getCities(): Observable<City[]> {
    return this.http.get<City[]>(API_URL + `/cities`, httpOptions);
  }

  getPointsOfRoute(routeId: number): Observable<RoutePoints> {
    return this.http.get<RoutePoints>(API_URL + `/${routeId}/points`, httpOptions);
  }

  getAllPoints(): Observable<Point[]> {
    return this.http.get<Point[]>(API_URL + `/points`, httpOptions);
  }

  getRouteById(id: number): Observable<Route> {
    return this.http.get<Route>(API_URL + `/${id}`, httpOptions);
  }

  getRouteByIdAsAdmin(id: number): Observable<Route> {
    return this.http.get<Route>(API_ADMIN_URL + `/${id}`, httpOptions);
  }

  public loadRoutes(
    size: number,
    page: number,
    sortFields?: Map<string, number>,
    filters?: Map<string, string>,
    onlyActive?: boolean,
  ): Observable<PageableResponse<Route>> {
    let params: HttpParams = SortingUtility.mapParams(size, page, sortFields, filters);
    params = RouteService.setOnlyActive(onlyActive, params);
    return this.http.get<PageableResponse<Route>>(API_ADMIN_URL, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public getAllByDateRange(startDate: Date, endDate: Date): Observable<Route[]> {
    const params: HttpParams = new HttpParams()
      .set('startDate', startDate.toLocaleDateString())
      .set('endDate', endDate.toLocaleDateString());
    return this.http.get<Route[]>(API_ADMIN_URL + '/date/range', {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public update(route: Route): Observable<Route> {
    return this.http.put<Route>(API_ADMIN_URL + `/${route.id}`, JSON.stringify(route), httpOptions);
  }

  public updateBus(routeId: number, busId: number): Observable<Route> {
    return this.http.put<Route>(API_ADMIN_URL + `/${routeId}/bus`, JSON.stringify(busId), httpOptions);
  }

  public updateDriver(routeId: number, driverId: number): Observable<Route> {
    return this.http.put<Route>(API_ADMIN_URL + `/${routeId}/driver`, JSON.stringify(driverId), httpOptions);
  }
}
