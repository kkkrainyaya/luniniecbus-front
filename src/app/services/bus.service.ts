import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PageableResponse} from '../entities/pageable-response';
import {Bus} from '../entities/bus';
import {environment} from '../../environments/environment';
import {SortingUtility} from '../helpers/sorting-utility';
import {DriverStatistics} from '../entities/driver-statistics';

const API_URL = environment.baseUrl + '/api/v1/buses';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class BusService {

  constructor(private http: HttpClient) {
  }

  public loadAll(
    size: number,
    page: number,
    sortField?: string,
    sortOrder?: number,
  ): Observable<PageableResponse<Bus>> {
    const params: HttpParams = SortingUtility.mapParams(size, page, new Map([[sortField, sortOrder]]), null);
    return this.http.get<PageableResponse<Bus>>(API_URL, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public update(bus: Bus): Observable<Bus> {
    return this.http.put<Bus>(API_URL, bus, httpOptions);
  }

  public delete(bus: Bus): Observable<void> {
    return this.http.post<void>(API_URL + `/delete/${bus.id}`, httpOptions);
  }

  public create(bus: Bus): Observable<Bus> {
    return this.http.post<Bus>(API_URL, bus, httpOptions);
  }

  public getBuses(): Observable<Bus[]> {
    return this.http.get<Bus[]>(API_URL + '/all', httpOptions);
  }

  public updateDriver(busId: number, driverId: number): Observable<Bus> {
    const params: HttpParams = new HttpParams()
      .set('driverId', driverId.toString());
    return this.http.put<Bus>(API_URL + `/${busId}/driver`, null, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public getStatistics(startDate: Date, endDate: Date): Observable<DriverStatistics[]> {
    const params: HttpParams = new HttpParams()
      .set('startDate', startDate.toLocaleDateString())
      .set('endDate', endDate.toLocaleDateString());
    return this.http.get<DriverStatistics[]>(API_URL + `/statistics`, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

}
