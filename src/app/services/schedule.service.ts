import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Schedule} from '../entities/schedule';
import {ScheduleMean} from '../entities/schedule-mean';
import * as moment from 'moment';
import {City} from '../entities/city';
import {Firm} from '../entities/firm';

const API_URL = environment.baseUrl + '/api/v1/schedules';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) {
  }

  public getAll(startDate: Date, endDate: Date): Observable<Schedule[]> {
    const params: HttpParams = new HttpParams()
      .set('startDate', startDate.toLocaleDateString())
      .set('endDate', endDate.toLocaleDateString());
    return this.http.get<Schedule[]>(API_URL + '/actual', {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public create(schedule: Schedule): Observable<Schedule> {
    return this.http.post<Schedule>(API_URL, JSON.stringify(schedule), httpOptions);
  }

  public update(schedule: Schedule): Observable<Schedule> {
    Date.prototype.toJSON = function(): string {
      return moment(this).format().split('+')[0];
    };
    return this.http.put<Schedule>(API_URL + `/${schedule.id}`, JSON.stringify(schedule), httpOptions);
  }

  public getPassengersAverage(statisticsStart: Date, statisticsEnd: Date): Observable<ScheduleMean[]> {
    const params: HttpParams = new HttpParams()
      .set('startDate', statisticsStart.toLocaleDateString())
      .set('endDate', statisticsEnd.toLocaleDateString());
    return this.http.get<ScheduleMean[]>(API_URL + '/statistics/average', {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  getFirms(): Observable<Firm[]> {
    return this.http.get<Firm[]>(API_URL + `/firms`, httpOptions);
  }

}
