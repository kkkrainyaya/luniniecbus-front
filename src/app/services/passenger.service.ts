import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Passenger} from '../entities/passenger';
import {PageableResponse} from '../entities/pageable-response';
import {SortingUtility} from '../helpers/sorting-utility';
import {PassengersStatistics} from '../entities/passengers-statistics';

const API_URL = environment.baseUrl + '/api/private/v1/passengers';
const API_ADMIN_URL = environment.baseUrl + '/api/private/v1/admin/passengers';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PassengerService {

  private static setOnlyActive(onlyActive: boolean, params: HttpParams): HttpParams {
    if (onlyActive) {
      const date = new Date();
      params = params.set('startTime', date.toLocaleTimeString())
        .set('startDate', date.toLocaleDateString());
    }
    return params;
  }

  private static setRoute(routeId: number, params: HttpParams): HttpParams {
    if (!!routeId) {
      params = params.set('routeId', `${routeId}`);
    }
    return params;
  }

  private static setUser(userId: number, params: HttpParams): HttpParams {
    if (!!userId) {
      params = params.set('userId', `${userId}`);
    }
    return params;
  }

  constructor(private http: HttpClient) {
  }

  public createPassenger(passenger: Passenger): Observable<Passenger> {
    return this.http.post<Passenger>(API_URL, JSON.stringify(passenger), httpOptions);
  }

  public createPassengerAsAdmin(passenger: Passenger): Observable<Passenger> {
    return this.http.post<Passenger>(API_ADMIN_URL, JSON.stringify(passenger), httpOptions);
  }

  public updatePassenger(passenger: Passenger): Observable<Passenger> {
    return this.http.put<Passenger>(API_URL + `/${passenger.id}`, JSON.stringify(passenger), httpOptions);
  }

  public updatePassengerAsAdmin(passenger: Passenger): Observable<Passenger> {
    return this.http.put<Passenger>(API_ADMIN_URL + `/${passenger.id}`, JSON.stringify(passenger), httpOptions);
  }

  public getPassengerById(id: number): Observable<Passenger> {
    return this.http.get<Passenger>(API_URL + `/${id}`, httpOptions);
  }

  public getPassengerByIdAsAdmin(id: number): Observable<Passenger> {
    return this.http.get<Passenger>(API_ADMIN_URL + `/${id}`, httpOptions);
  }

  public deletePassenger(id: number): Observable<object> {
    return this.http.delete(API_URL + `/${id}`, httpOptions);
  }

  public reorder(id: number, passenger: Passenger): Observable<Passenger> {
    return this.http.post<Passenger>(API_ADMIN_URL + `/${id}/reorder`, passenger, httpOptions);
  }

  public loadPassengers(
    size: number,
    page: number,
    sortField?: string,
    sortOrder?: number,
    filters?: Map<string, string>,
    routeId?: number,
    userId?: number,
    onlyActive?: boolean,
  ): Observable<PageableResponse<Passenger>> {
    let params: HttpParams = SortingUtility.mapParams(size, page, new Map([[sortField, sortOrder]]), filters);
    params = PassengerService.setRoute(routeId, params);
    params = PassengerService.setUser(userId, params);
    params = PassengerService.setOnlyActive(onlyActive, params);
    return this.http.get<PageableResponse<Passenger>>(API_ADMIN_URL, {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public isActive(passenger: Passenger): boolean {
    return new Date(passenger.route.departureDateTime).getTime() > new Date().getTime();
  }

  public updatePassengerConfirmation(passengerId: number, confirmed: boolean): Observable<Passenger> {
    return this.http.put<Passenger>(API_ADMIN_URL + `/${passengerId}/confirmation`, JSON.stringify(confirmed), httpOptions);
  }

  public getStatisticsByRouteDate(statisticsStart: Date, statisticsEnd: Date): Observable<PassengersStatistics> {
    const params: HttpParams = new HttpParams()
      .set('startDate', statisticsStart.toLocaleDateString())
      .set('endDate', statisticsEnd.toLocaleDateString());
    return this.http.get<PassengersStatistics>(API_ADMIN_URL + '/statistics/route-date', {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  public getStatisticsByCreationDate(statisticsStart: Date, statisticsEnd: Date): Observable<PassengersStatistics> {
    const params: HttpParams = new HttpParams()
      .set('startDate', statisticsStart.toLocaleDateString())
      .set('endDate', statisticsEnd.toLocaleDateString());
    return this.http.get<PassengersStatistics>(API_ADMIN_URL + '/statistics/creation-date', {
      params,
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }
}
